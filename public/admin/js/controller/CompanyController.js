OrdantApp.controller('CompanyController', function($rootScope, $scope, $window, $http, $mdDialog, $compile, commonServices){
	$scope.limit 			= 10;
	$scope.currentPage 		= 1;
	$scope.next_page_url 	= '';
	$scope.horscopeList	= [];
	$scope.contactEnquire 	= [];
	$scope.params = {};




/* get records by parameters */
const  getHoroscope = () => {
    if($scope.next_page_url != ''){
        $scope.params.page = $scope.currentPage;
    } else {
        $scope.params.page = 1;
    }
    
    $scope.show_loading = true;
   
     $scope.fromService = commonServices.getList(BASE_URL+'api/company_list', $scope.params).then(function(result) {
        $scope.horscopeList      =result.data.data.data;
        $scope.paging           = result.data;
        $scope.currentPage      = $scope.paging.current_page,
        $scope.numPerPage       = $scope.paging.per_page,
        $scope.maxSize          = 5;
        $scope.totalItems       = $scope.paging.total;
        $scope.next_page_url    = $scope.paging.next_page_url;
        $scope.prev_page_url    = $scope.paging.prev_page_url;

        $scope.show_loading = false;
        submit = $('#search_button');
        if($(submit).hasClass('disable'))
        {
            $(submit).removeClass('disable');
            $(submit).removeAttr('disabled');
            $(submit).val('Submit');
        }
       
       
    });

    
}

/* get records first time */
$scope.getcompanyList = function(){
  
    $scope.limit = 10;
    $scope.currentPage 	= 1;
    getHoroscope();
  
};

/* get records on page change */
$scope.pageContactChanged = function() {
    getHoroscope();
};


$scope.getsalespersonList = function(){
  
    $scope.fromService = commonServices.getList(BASE_URL+'api/sales_person_list').then(function(result) {
        $scope.salespersons =result.data.data;
       
       
    });
  
  
};


$scope.getcountryList = function(){     
    $scope.fromService = commonServices.getList(BASE_URL+'api/country_list').then(function(result) {
           $scope.countries      =result.data.data;
          
          //console.log($scope.products  )
          
       });
     
   };

$scope.addCompany = function(ev) {
    var $this=$('#add_new_btn');
	var $defaultText=$this.text();
    var $this=$('#add_new_btn');
	var $defaultText=$this.text();
	var requestcallbackurl=BASE_URL+'api/insert_company';
	var form=$('#add_company_form')[0];
	var formData=new FormData();
    formData.append('name',$('#name').val());
    formData.append('country',$('#country').val());
    formData.append('email',$('#email').val());
    formData.append('website',$('#website').val());
    formData.append('linkedin_profile',$('#linkedin_profile').val());
    formData.append('facebook_url',$('#facebook_url').val());
    formData.append('head_office_number',$('#head_office_number').val());
    formData.append('secondary_contact_number',$('#secondary_contact_number').val());
    formData.append('sales_person',$('#sales_person').val());
    formData.append('status',$('#status').val());
    formData.append('created_by',$('#created_by').val());
    formData.append('notes',$('#notes').val());
	

    if($('#add_company_form').valid()){
        $this.addClass('disable');
        $this.attr('disable',true);
        $this.text('Please Wait...');

        $http({
            method  : 'POST',
            url     : requestcallbackurl,
            data    : formData, //forms user object
            headers : {'Content-Type':undefined}

        }).then(function(response) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('body')))
                .clickOutsideToClose(false)
                .title('')
                .textContent(response.data.message)
                .ariaLabel('')
                .ok('Ok')
                .targetEvent(ev)
            ).finally(function() {
                if(response.data.status != false){
                    $window.location.reload();
                }
                
            });

           
                
        });
 
    }

};


$scope.editCompany = function(ev) {
    var $this=$('#edit_new_btn');
    var $defaultText=$this.text();
    var requestcallbackurl=BASE_URL+'api/company_update';
    
    var formData=new FormData();
    formData.append('id',$('#id').val());
    formData.append('name',$('#name').val());
    formData.append('country',$('#country').val());
    formData.append('email',$('#email').val());
    formData.append('website',$('#website').val());
    formData.append('linkedin_profile',$('#linkedin_profile').val());
    formData.append('facebook_url',$('#facebook_url').val());
    formData.append('head_office_number',$('#head_office_number').val());
    formData.append('secondary_contact_number',$('#secondary_contact_number').val());
    formData.append('sales_person',$('#sales_person').val());
    formData.append('status',$('#status').val());
    formData.append('created_by',$('#created_by').val());
    formData.append('notes',$('#notes').val());
    if($('#edit_company_form').valid()){
        $this.addClass('disable');
        $this.attr('disable',true);
        $this.text('Please Wait...');

        $http({
            method  : 'PUT',
            url     : requestcallbackurl,
            data    : formData, //forms user object
            headers : {'Content-Type':undefined}

        }).then(function(response) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('body')))
                .clickOutsideToClose(false)
                .title('')
                .textContent(response.data.message)
                .ariaLabel('')
                .ok('Ok')
                .targetEvent(ev)
            ).finally(function() {
                if(response.data.status != false){
                    $window.location.href = BASE_URL+'companies';
                }
                
            });

           
                
        });
    }
};

});


