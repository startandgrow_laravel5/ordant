
TscthAPP.directive('dropdownList', ['$filter', function($filter){
    return {
        restrict: 'E',
        require:'^ngModel',
        scope: {
            items: '=',
            ngModel: '=',
            mainList: '=',
        },
        templateUrl: base_url+'/admin/js/controllers/get-drop-down-list.html',
        link: function (scope, element, attrs, ngModelCtrl) {       

            scope.main_label      = attrs.name;
            scope.select_label    = attrs.label;
            scope.css_class       = attrs.class;
            scope.css_id          = attrs.id;
            scope.label_keyword   = attrs.keyword;            

            /* display or hide search box inside blade*/
            if (attrs.search=='no') {
                scope.show_search ='0';
            }else{
                scope.show_search = '1';
            }

            /* display or hode checkbox inside blade*/
            if (attrs.multiple=='no') {
                scope.show_multiple ='0';
            }else{
                scope.show_multiple = '1';
            }
            /* display or hode checkbox inside blade*/
            if (attrs.multiselect=='no') {
                scope.show_multiselect ='0';
            }else{
                scope.show_multiselect = '1';
            }

            /* Set a keyword for all drop down section inside blade*/
            scope.label_keyword = attrs.keyword; 
            
            /* check all record */
            scope.checkAllRecord = function(scope){
                
                var text = '';

                if (scope.select_all_list) {
                    
                    for(var i = 0; i < scope.items.length; i++) {
                        
                        scope.ngModel.push(scope.items[i].id);

                        text += scope.items[i].search_word+', ';
                    }                    

                }else{
                    
                    if ($('#search_'+scope.label_keyword).val()=='') {
                        
                        scope.ngModel.length=0;
                    }
                }

                setTimeout(function(){
                    $.each($('.'+attrs.keyword+'-selection-list-checkbox'), function(key, element){
                        
                        if($('#check_all_'+attrs.keyword).is(':checked')){
                            
                            $(element).prop('checked', 'checked');

                        } else {

                            $(element).removeAttr('checked');
                        }
                    });
                }, 100);  

                /*add titles in drop down comma seprated*/
                scope.addSelectedElementLabelInDropdown(text);  
                /* remove duplicate value from array */
                scope.removeDuplicateIndexFromSelectedArray();
                /* this function will get the data on click of checkbox */
                
                if (attrs.useClickCallBack=='yes') {
                    
                    var function_type = attrs.clickCallback;

                    scope.$emit(function_type, scope); 
                      
                } 
                /*console.log('list');
                console.log(scope.items);
                console.log('model');
                console.log(scope.ngModel);
                console.log('all');
                console.log(scope.mainList);*/
                  
                
            }// check all record end

            /* check and uncheck single record */
            scope.checkUncheckCheckbox = function(scope){                
                
                var text = '';
                
                if (scope.list_item) {

                    scope.ngModel.push(scope.record.id);

                }else{
                    
                     scope.select_all_list = false;

                    $('#check_all_'+attrs.keyword).removeAttr('checked');

                    for( var i = 0; i < scope.ngModel.length; i++){
                        if ( scope.ngModel[i] == scope.record.id) {
                            scope.ngModel.splice(i, 1); 
                        }
                    }
                }

                /* remvoe duplicate value from selected array */
                scope.removeDuplicateIndexFromSelectedArray();

                /* set data in root scope to get in child controller */
                text = scope.getAllSelectedElementLabels(text);                
                
                /* add titles in drop down comma seprated */
                scope.addSelectedElementLabelInDropdown(text);
                
                /* checked all record after filter*/
                scope.checkUncheckAllRecord();
                
                /* this function will get the data on click of checkbox */
                if (attrs.useClickCallBack=='yes') {
                    var function_type = attrs.clickCallback;

                    scope.$emit(function_type, scope);
                }
                /*console.log('list');
                console.log(scope.items);
                console.log('model');
                console.log(scope.ngModel);
                console.log('all');
                console.log(scope.mainList);*/
                
            }//toggleCheckRecord end 

            /* check and uncheck single record */
            scope.checkUncheckRadio = function(scope){
                var text = '';                
                
                scope.ngModel[0]=scope.record.id; 

                /* set data in root scope to get in child controller */
                text = scope.getAllSelectedElementLabels(text);                
                
                /* add titles in drop down comma seprated */
                scope.addSelectedElementLabelInDropdown(text);               
                
                /* this function will get the data on click of checkbox */
                if (attrs.useClickCallBack=='yes') {
                    var function_type = attrs.clickCallback;

                    scope.$emit(function_type, scope);
                }                

                
            }//toggleCheckRecord end        

            /* check and unchecked all record*/
            scope.checkUncheckAllRecord = function(){
                
                setTimeout(function(){

                    if (scope.ngModel.length>0) {

                        for(var i = 0; i < scope.ngModel.length; i++) {

                             $( '#check'+attrs.keyword +scope.ngModel[i] ).prop('checked', 'checked');
                        }
                    }
                }, 100);    
            }

            /*add titles in drop down comma seprated*/
            scope.addSelectedElementLabelInDropdown = function(text){
                if (text!='') {
                    $('#'+attrs.keyword+'-anchor-label').text(removeLastComma(text));    
                }else{
                    $('#'+attrs.keyword+'-anchor-label').text(attrs.label);
                }
            }

            /* get all selected element labels */
            scope.getAllSelectedElementLabels = function(text){
                
                if (scope.ngModel.length>0) {

                    for (var i = 0; i < scope.items.length; i++) {
                        
                        if (scope.ngModel.includes(scope.items[i].id)) {
                            text += scope.items[i].search_word+', ';
                        }
                    }
                }
                return text;
            }

            /* remvoe duplicate value from selected array */
            scope.removeDuplicateIndexFromSelectedArray = function(){
               
                scope.ngModel = scope.ngModel.filter( function( item, index, inputArray ) {
                       return inputArray.indexOf(item) == index;
                });
            }            

        },
        controller: function($rootScope, $scope, $http, $attrs, $parse) {  
            
            /* Get drop down data from database for all drop down on load page */
            $scope.main_list_all_records=[];

            if ($attrs.providerType=='dataset') {

                if ($attrs.provider!='') {
                    
                    var storedArray = JSON.parse($attrs.provider); 
                    var data = []; 

                    Object.keys(storedArray).forEach(function(key) {
                        
                        data.push({'id':key,'search_word':storedArray[key]});                    

                    });

                    $scope.items = data;
                    $scope.mainList = data;    
                }
                

            }else{

                var function_type = $attrs.provider; // this function will get the data on page load

                /* it will set data in root scope */
                $scope.$emit(function_type, 'load');
                
                $scope.mainList=$scope.items;
            }
            /*console.log('list');
            console.log($scope.items);
            console.log('model');
            console.log($scope.ngModel);
            console.log('all');
            console.log($scope.mainList);*/

            /* accept the data from brodcast */
            $scope.$on("SendDownToSelf", function (evt, data) {
                /*console.log('SendDownToSelf in controller');
                console.log($scope);*/
                //if (data.length>0) {
                   /* for (var i = 0; i < data.length; i++) {
                        //$scope.items.push(data[i]);
                        //$scope.mainList.push(data[i]);
                    }*/
                //}
                
            });

            /* function to filter record from drop down */
            $scope.searchResult = function($childScope){                  

                //$scope.select_all_list = false;
                //$('#check_all_'+$attrs.keyword).removeAttr('checked');

                var search_array =[];

                var main_record = $scope.mainList;
                
                if ($attrs.useSearchCallBack=='no') {

                    /*console.log('first search');
                    console.log('list');
                    console.log($scope.items);
                    console.log('model');
                    console.log($scope.ngModel);
                    console.log('all');
                    console.log($scope.mainList);*/

                    if($scope.mainList.length > 0 && $childScope.search_key!=''){
                        

                        $.each($scope.mainList, function(k, list){
                            
                            if( $childScope.search_key != '' && list.search_word.toLowerCase().indexOf($childScope.search_key) != -1){
                               search_array.push(list);
                            }
                        });
                        
                        $scope.items=[];
                        $scope.items= search_array;
                        $scope.mainList= main_record; 

                    }else{
                        
                        $scope.items=$scope.mainList;
                    }
                    /*console.log('second search');
                    console.log('list');
                    console.log($scope.items);
                    console.log('model');
                    console.log($scope.ngModel);
                    console.log('all');
                    console.log($scope.mainList);*/

                }else{                   
                    
                    /* below code will work on search of result */
                    var search_key = $childScope.search_key;
                    function_type = $attrs.searchCallback;

                    $scope.$emit(function_type,search_key); 
                    //$scope.mainList=$scope.items;
                }

                $scope.checkAllSearchAndPrevRecord();                
                               
            }//end search function 

            /* check and unchecked all record*/
            $scope.checkAllSearchAndPrevRecord = function(){
               
                setTimeout(function(){

                    if ($scope.ngModel.length>0) {

                        for(var i = 0; i < $scope.ngModel.length; i++) {

                             $( '#check'+$attrs.keyword +$scope.ngModel[i] ).prop('checked', 'checked');
                        }
                    }
                }, 100);    
            }
            $scope.vsRepeatTrigger = function(){ 
                console.log('scroll-to-top');                             
                $('.repeater-container').scrollTop(0);
            }
        }//end controller
    };// end return
  }]);