OrdantApp.controller('DashboardController', function($rootScope, $scope, $window, $http, $mdDialog, $compile, commonServices){
	$scope.limit 			= 10;
	$scope.currentPage 		= 1;
	$scope.next_page_url 	= '';
	$scope.horscopeList 	= [];
	$scope.contactEnquire 	= [];
	$scope.params = {};



/* get records by parameters */
const  getHoroscope = () => {
    if($scope.next_page_url != ''){
        $scope.params.page = $scope.currentPage;
    } else {
        $scope.params.page = 1;
    }
    
    $scope.show_loading = true;
   
     $scope.fromService = commonServices.getList(BASE_URL+'api/user_dashboard', $scope.params).then(function(result) {
        $scope.dasboardcount      =result.data.data;
        $scope.dasboardcompanies      =result.data.data.companies;
		console.log($scope.dasboardcount );
       
    });

    
}

/* get records first time */
$scope.getDashboardList = function(){
  
    $scope.limit = 10;
    $scope.currentPage 	= 1;
    getHoroscope();
  
};

	/* get records on page change */
	$scope.pageContactChanged = function() {
		getHoroscope();
	};
/* get records by parameters */
	const  getEnquire = () => {
		$scope.params.limit = 10;
		if($scope.next_page_url != ''){
			$scope.params.page = $scope.currentPage;
		} else {
			$scope.params.page = 1;
		}
		

        $scope.fromService = commonServices.getList(api_url+'/getEnquireList', $scope.params).then(function(result) {
            $scope.contactEnquire      = result.data.data;
            $scope.paging           = result.data;
            $scope.currentPage      = $scope.paging.current_page,
            $scope.numPerPage       = $scope.paging.per_page,
            $scope.maxSize          = 5;
            $scope.totalItems       = $scope.paging.total;
            $scope.next_page_url    = $scope.paging.next_page_url;
            $scope.prev_page_url    = $scope.paging.prev_page_url;

           
        });
	
		
	}

	/* get records first time */
	$scope.getEnquireList = function(){
		$scope.limit 		= 10;
		$scope.currentPage 	= 1;
		getEnquire();
	};

	/* get records on page change */
	$scope.pageEnqChanged = function() {
		getEnquire();
	};


$scope.addHoroscope = function(ev) {

	var $this=$('#add_new_btn');
	var $defaultText=$this.text();
	var requestcallbackurl=api_url+'/addProduct';
	var form=$('#add_horoscope_form')[0];
	var formData=new FormData();
    formData.append('name',$('#name').val());
    formData.append('quantity',$('#quantity').val());
    formData.append('weight',$('#weight').val());
    formData.append('measurment',$('#measurment').val());
    formData.append('mrp_price',$('#mrp_price').val());
    formData.append('manufacturing_date',$('#manufacturing_date').val());
    formData.append('expire_date',$('#expire_date').val());
    formData.append('sort_order',$('#sort_order').val());
    formData.append('status',$('#status').val());

    if(document.getElementById('icon') != undefined && document.getElementById('icon').files != undefined && document.getElementById('icon').files[0] != undefined){
    	formData.append('icon',document.getElementById('icon').files[0]);
    }

    if(document.getElementById('image') != undefined && document.getElementById('image').files != undefined && document.getElementById('image').files[0] != undefined){
    	formData.append('image',document.getElementById('image').files[0]);
    }

    content = CKEDITOR.instances['description'];
    if(content != undefined){
    	formData.append('description',content.getData());
    }



	//console.log($defaultText);

		if($('#add_horoscope_form').valid()){
			$this.addClass('disable');
			$this.attr('disable',true);
			$this.text('Please Wait...');

			$http({
				method  : 'POST',
				url     : requestcallbackurl,
				data    : formData, //forms user object
				headers : {'Content-Type':undefined}

			}).then(function(response) {
				return false;
					$mdDialog.show(
							$mdDialog.alert()
							.parent(angular.element(document.querySelector('body')))
							.clickOutsideToClose(false)
							.title('')
							.textContent(response.data.message)
							.ariaLabel('')
							.ok('Ok')
							.targetEvent(ev)
						).finally(function() {
							if(response.data.status != false){
								$window.location.reload();
							}
							
						});
			});
		}

};

$scope.editHoroscope = function(ev) {

    var $this=$('#edit_new_btn');
    var $defaultText=$this.text();
    var requestcallbackurl=api_url+'/editProduct';
    
    var formData=new FormData();
    formData.append('id',$('#id').val());
    formData.append('name',$('#name').val());
    formData.append('quantity',$('#quantity').val());
    formData.append('weight',$('#weight').val());
    formData.append('measurment',$('#measurment').val());
    formData.append('mrp_price',$('#mrp_price').val());
    formData.append('manufacturing_date',$('#manufacturing_date').val());
    formData.append('expire_date',$('#expire_date').val());
    formData.append('sort_order',$('#sort_order').val());
    formData.append('status',$('#status').val());

    if(document.getElementById('icon') != undefined && document.getElementById('icon').files != undefined && document.getElementById('icon').files[0] != undefined){
    	formData.append('icon',document.getElementById('icon').files[0]);
    }

    if(document.getElementById('image') != undefined && document.getElementById('image').files != undefined && document.getElementById('image').files[0] != undefined){
    	formData.append('image',document.getElementById('image').files[0]);
    }

    content = CKEDITOR.instances['description'];
    if(content != undefined){
    	formData.append('description',content.getData());
    }

    //console.log($defaultText);

        if($('#edit_horoscope_form').valid()){
            $this.addClass('disable');
            $this.attr('disable',true);
            $this.text('Please Wait...');

            $http({
                method  : 'POST',
                url     : requestcallbackurl,
                data    : formData, //forms user object
                headers : {'Content-Type':undefined}

            }).then(function(response) {
                    $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.querySelector('body')))
                            .clickOutsideToClose(false)
                            .title('')
                            .textContent(response.data.message)
                            .ariaLabel('')
                            .ok('Ok')
                            .targetEvent(ev)
                        ).finally(function() {
                            if(response.data.status != false){
                                $window.location.reload();
                            }
                            
                        });
            });
        }

};


$scope.addProductCode = function(ev) {

	var $this=$('#add_new_btn');
	var $defaultText=$this.text();
	var requestcallbackurl=api_url+'/addProductCode';
	var form=$('#add_product_code_form')[0];
	var formData=new FormData();
    formData.append('product_id',$('#name').val());
    formData.append('quantity',$('#quantity').val());
    formData.append('weight',$('#weight').val());
    formData.append('measurment',$('#measurment').val());
    formData.append('mrp_price',$('#mrp_price').val());
    formData.append('manufacturing_date',$('#manufacturing_date').val());
    formData.append('expire_date',$('#expire_date').val());
    formData.append('sort_order',$('#sort_order').val());
    formData.append('status',$('#status').val());

    if(document.getElementById('icon') != undefined && document.getElementById('icon').files != undefined && document.getElementById('icon').files[0] != undefined){
    	formData.append('icon',document.getElementById('icon').files[0]);
    }

    if(document.getElementById('image') != undefined && document.getElementById('image').files != undefined && document.getElementById('image').files[0] != undefined){
    	formData.append('image',document.getElementById('image').files[0]);
    }

    content = CKEDITOR.instances['description'];
    if(content != undefined){
    	formData.append('description',content.getData());
    }



	//console.log($defaultText);

		if($('#add_horoscope_form').valid()){
			$this.addClass('disable');
			$this.attr('disable',true);
			$this.text('Please Wait...');

			$http({
				method  : 'POST',
				url     : requestcallbackurl,
				data    : formData, //forms user object
				headers : {'Content-Type':undefined}

			}).then(function(response) {
				return false;
					$mdDialog.show(
							$mdDialog.alert()
							.parent(angular.element(document.querySelector('body')))
							.clickOutsideToClose(false)
							.title('')
							.textContent(response.data.message)
							.ariaLabel('')
							.ok('Ok')
							.targetEvent(ev)
						).finally(function() {
							if(response.data.status != false){
								$window.location.reload();
							}
							
						});
			});
		}

};

/****/
});


