OrdantApp.controller('QuoteController', function($rootScope, $scope, $window, $http, $mdDialog, $compile, commonServices){
	$scope.limit 			= 10;
	$scope.currentPage 		= 1;
	$scope.next_page_url 	= '';
	$scope.horscopeList	= [];
	$scope.contactEnquire 	= [];
	$scope.params = {};
    $scope.formData = {};




/* get records by parameters */
const  getHoroscope = () => {
    if($scope.next_page_url != ''){
        $scope.params.page = $scope.currentPage;
        $scope.userId = 1;
    } else {
        $scope.params.page = 1;
    }
    
    
   
     $scope.fromService = commonServices.getList(BASE_URL+'api/quote_list/1', $scope.params).then(function(result) {
        $scope.horscopeList      =result.data.data.data;
        $scope.paging           = result.data;
        $scope.currentPage      = $scope.paging.current_page,
        $scope.numPerPage       = $scope.paging.per_page,
        $scope.maxSize          = 5;
        $scope.totalItems       = $scope.paging.total;
        $scope.next_page_url    = $scope.paging.next_page_url;
        $scope.prev_page_url    = $scope.paging.prev_page_url;
        submit = $('#search_button');
        if($(submit).hasClass('disable'))
        {
            $(submit).removeClass('disable');
            $(submit).removeAttr('disabled');
            $(submit).val('Submit');
        }
          console.log($scope.horscopeList  )
       
    });

    
}

/* get records first time */
$scope.getquoteList = function(){
  
    $scope.limit = 10;
    $scope.currentPage 	= 1;
    getHoroscope();
  
};

$scope.getproductList = function(){     
 $scope.fromService = commonServices.getList(BASE_URL+'api/products', $scope.params).then(function(result) {
        $scope.products      =result.data.data.data;
       
       //console.log($scope.products  )
       
    });
  
};

$scope.getcustomertList = function(){     
    $scope.fromService = commonServices.getList(BASE_URL+'api/contacts').then(function(result) {
           $scope.users =result.data.data.data;
          
         
          
       });
     
   };

   $scope.getsalestList = function(){     
    $scope.fromService = commonServices.getList(BASE_URL+'api/sales_person_list').then(function(result) {
           $scope.salesusers =result.data.data;
          
          
          
       });
     
   };

   $scope.getcompanyList = function(){     
    $scope.fromService = commonServices.getList(BASE_URL+'api/company_list').then(function(result) {
           $scope.companies =result.data.data.data;
          
        
          
       });
     
   };

   $scope.companySelected = function(){     
    $scope.params.id = $scope.selectedCompany;
    $scope.fromService = commonServices.getList(BASE_URL+'api/company_detail/'+$scope.selectedCompany).then(function(result) {
        $scope.companydetails =result.data.data;
    
       
    });
    
     
   };


   
    //quote page  product search
    $scope.ProductSuggest = function($event)
    
    {
        $scope.term = $scope.searchTest;
        
        //alert($scope.term)

        if(typeof $scope.searchTest  == "undefined"  || $scope.searchTest == "")
        {
            $scope.products = {};

        }
        else
        {
            $scope.fromService = commonServices.getList(BASE_URL+'api/products', $scope.params).then(function(result) {
                $scope.products      =result.data.data.data;
               
              console.log($scope.products);
               
            });

        }
           
     

    }

    $scope.setProduct = function(index,$event){
        $scope.searchTest = $scope.products[index].name;
        $scope.product_price = $scope.products[index].product_price;
        $scope.vendor_price = $scope.products[index].vendor_price;
        $scope.product_name = $scope.products[index].product_name;
        $scope.quantity = $scope.products[index].quantity;
        $scope.product_id = $scope.products[index].id;
        $scope.products = {};
        $event.stopPropagation();
     }



/* get records on page change */
$scope.pageContactChanged = function() {
    getHoroscope();
};

$scope.getComment = function(horscope) {
    //alert(horscope);
    $scope.quoteId=horscope;
    $scope.fromService = commonServices.getList(BASE_URL+'api/order_detail/'+$scope.quoteId).then(function(result) {
        $scope.quotedetails =result.data.data.order;
        $scope.quotecomments =result.data.data.comments;
    
       //console.log($scope.quotecomments);
    });
   


};

$scope.changeStatus = function(ev) {

    var $this=$('#add_new_btn');
	var $defaultText=$this.text();
    var $this=$('#add_new_btn');
	var $defaultText=$this.text();
	var requestcallbackurl=BASE_URL+'api/quote_status_update';
	var form=$('#add_comment_form')[0];
	var formData=new FormData();
    formData.append('status',$('#status').val());
    formData.append('order_id',$('#order_id').val());
    formData.append('customer_id',$('#customer_id').val());
    formData.append('comments',$('#comments').val());
   
    if($('#add_comment_form').valid()){
        $this.addClass('disable');
        $this.attr('disable',true);
        $this.text('Please Wait...');

        $http({
            method  : 'POST',
            url     : requestcallbackurl,
            data    : formData, //forms user object
            headers : {'Content-Type':undefined}

        }).then(function(response) {
            $scope.data =response.data.message;

            alert($scope.data);
            $window.location.href = BASE_URL+'quotes';
        });
    }
};

$scope.addQuotes = function(ev) {
    $scope.formData.qty  =  $scope.quantity;
    $scope.formData.product_id =$scope.product_id;
    $scope.formData.supl_id =$scope.selectedCompany;
    $scope.formData.cust_id =$scope.cust_id;
    $scope.formData.total =$scope.quantity*$scope.product_price;
    console.log( $scope.formData);
    if ($scope.formData != undefined ) {

        $http({
        method  : 'post',
        url     : BASE_URL+'api/order_insert',
        data    : $scope.formData,
        }).then(function (data) {
        $scope.success = data.data.data;
        if ($scope.success != undefined ) {
         alert('QUOTE Added Succesfully');
         $window.location.href = BASE_URL+'quotes';
    

        }
        else{
          alert('please check !!Try Again');
        }
       
        });

         
       }

};


$scope.editCompany = function(ev) {
    var $this=$('#edit_new_btn');
    var $defaultText=$this.text();
    var requestcallbackurl=BASE_URL+'api/company_update';
    
    var formData=new FormData();
    formData.append('id',$('#id').val());
    formData.append('name',$('#name').val());
    formData.append('country',$('#country').val());
    formData.append('email',$('#email').val());
    formData.append('website',$('#website').val());
    formData.append('linkedin_profile',$('#linkedin_profile').val());
    formData.append('facebook_url',$('#facebook_url').val());
    formData.append('head_office_number',$('#head_office_number').val());
    formData.append('secondary_contact_number',$('#secondary_contact_number').val());
    formData.append('sales_person',$('#sales_person').val());
    formData.append('status',$('#status').val());
    formData.append('created_by',$('#created_by').val());
    formData.append('notes',$('#notes').val());
    if($('#edit_company_form').valid()){
        $this.addClass('disable');
        $this.attr('disable',true);
        $this.text('Please Wait...');

        $http({
            method  : 'PUT',
            url     : requestcallbackurl,
            data    : formData, //forms user object
            headers : {'Content-Type':undefined}

        }).then(function(response) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('body')))
                .clickOutsideToClose(false)
                .title('')
                .textContent(response.data.message)
                .ariaLabel('')
                .ok('Ok')
                .targetEvent(ev)
            ).finally(function() {
                if(response.data.status != false){
                    $window.location.href = BASE_URL+'companies';
                }
                
            });

           
                
        });
    }
};

});


