var OrdantApp = angular.module("OrdantApp", ['ui.bootstrap', 'ngMaterial','ngSanitize'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<@');
    $interpolateProvider.endSymbol('@>');
});


OrdantApp.service('commonServices',function($http, $location, $mdDialog){
    /** 
    * get records list 
    * url: ajax call url 
    * params: params to be sent with request
    */
    this.getList = function(url, params){
        var query_params = '';
        $.each(params, function(k,v){
            if(query_params != ''){
                query_params = query_params+'&'+k+'='+v;
            } else {
                query_params = '?'+k+'='+v;
            }
        });
        url = url+query_params;
        return $http({
            method: 'GET',
            url: url
        });
    };
});