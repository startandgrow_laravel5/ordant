OrdantApp.controller('EmailTemplateController', function($rootScope, $scope, $window, $http, $mdDialog, $compile, commonServices){
	$scope.limit 			= 10;
	$scope.currentPage 		= 1;
	$scope.next_page_url 	= '';
	$scope.TemplateList	= [];
	$scope.params = {};


/* get records by parameters */
const  getEmailTemplate = () => {
    if($scope.next_page_url != ''){
        $scope.params.page = $scope.currentPage;
    } else {
        $scope.params.page = 1;
    }
    $scope.show_loading = true;   
     $scope.fromService = commonServices.getList(BASE_URL+'api/template-list', $scope.params).then(function(result) {
        $scope.TemplateList      =result.data.data.data;
        $scope.paging           = result.data;
        $scope.currentPage      = $scope.paging.current_page,
        $scope.numPerPage       = $scope.paging.per_page,
        $scope.maxSize          = 5;
        $scope.totalItems       = $scope.paging.total;
        $scope.next_page_url    = $scope.paging.next_page_url;
        $scope.prev_page_url    = $scope.paging.prev_page_url;
        $scope.show_loading = false;
        submit = $('#search_button');
        if($(submit).hasClass('disable'))
        {
            $(submit).removeClass('disable');
            $(submit).removeAttr('disabled');
            $(submit).val('Submit');
        }
       
       
    });

    
}

/* get records first time */
$scope.getEmailTemplateList = function(){
  
    $scope.limit = 10;
    $scope.currentPage 	= 1;
    getEmailTemplate();
  
};


/* get records on page change */
$scope.pageTemplateChanged = function() {
    getEmailTemplate();
};

$scope.addTemplate = function(ev) {
    var $this=$('#add_new_btn');
	var $defaultText=$this.text();
    var $this=$('#add_new_btn');
	var $defaultText=$this.text();
	var requestcallbackurl=BASE_URL+'api/add-emailtemplate';
	var frm = $('#add_template_form');
    var formData = new FormData(frm[0]);
    content = CKEDITOR.instances['content'];
    if(content != undefined){
        formData.append('content',content.getData());
    }
    if($('#add_template_form').valid()){
        $this.addClass('disable');
        $this.attr('disable',true);
        $this.text('Please Wait...');
        $http({
            method  : 'POST',
            url     : requestcallbackurl,
            data    : formData, //forms user object
            headers : {'Content-Type':undefined}

        }).then(function(response) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('body')))
                .clickOutsideToClose(false)
                .title('')
                .textContent(response.data.message)
                .ariaLabel('')
                .ok('Ok')
                .targetEvent(ev)
            ).finally(function() {
                if(response.data.status != false){
                    $window.location.href = BASE_URL+'email-template';
                }
                
            });
            
        });
    }

};


$scope.editTemplate = function(ev) {
    var $this=$('#edit_new_btn');
    var $defaultText=$this.text();
    var requestcallbackurl=BASE_URL+'api/edit-emailtemplate';
    
    var frm = $('#edit_template_form');
    var formData = new FormData(frm[0]);
    content = CKEDITOR.instances['content'];
    if(content != undefined){
        formData.append('content',content.getData());
    }
    if($('#edit_template_form').valid()){
        $this.addClass('disable');
        $this.attr('disable',true);
        $this.text('Please Wait...');

        $http({
            method  : 'POST',
            url     : requestcallbackurl,
            data    : formData, //forms user object
            headers : {'Content-Type':undefined}

        }).then(function(response) {
            $this.removeClass('disable');
            $this.attr('disable',false);
            $this.text('Update');
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('body')))
                .clickOutsideToClose(false)
                .title('')
                .textContent(response.data.message)
                .ariaLabel('')
                .ok('Ok')
                .targetEvent(ev)
            ).finally(function() {
                // if(response.data.status != false){
                //     $window.location.href = BASE_URL+'email-template';
                // }
                
            });

           
                
        });
    }
};

});


