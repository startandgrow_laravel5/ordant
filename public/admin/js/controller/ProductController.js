OrdantApp.controller('ProductController', function($rootScope, $scope, $window, $http, $mdDialog, $compile, commonServices){
	$scope.limit 			= 10;
	$scope.currentPage 		= 1;
	$scope.next_page_url 	= '';
	$scope.horscopeList	= [];
	$scope.contactEnquire 	= [];
	$scope.params = {};




/* get records by parameters */
const  getProduct = () => {
    if($scope.next_page_url != ''){
        $scope.params.page = $scope.currentPage;
    } else {
        $scope.params.page = 1;
    }
       
     $scope.fromService = commonServices.getList(BASE_URL+'api/products', $scope.params).then(function(result) {
        $scope.horscopeList      =result.data.data.data;
        $scope.paging           = result.data;
        $scope.currentPage      = $scope.paging.current_page,
        $scope.numPerPage       = $scope.paging.per_page,
        $scope.maxSize          = 5;
        $scope.totalItems       = $scope.paging.total;
        $scope.next_page_url    = $scope.paging.next_page_url;
        $scope.prev_page_url    = $scope.paging.prev_page_url;
        submit = $('#search_button');
        if($(submit).hasClass('disable'))
        {
            $(submit).removeClass('disable');
            $(submit).removeAttr('disabled');
            $(submit).val('Submit');
        }
       
       
    });

    
}

/* get records first time */
$scope.getproductList = function(){
  
    $scope.limit = 10;
    $scope.currentPage 	= 1;
    getProduct();
  
};

/* get records on page change */
$scope.pageContactChanged = function() {
    getProduct();
};

$scope.addProduct = function(ev) {
    var $this=$('#add_new_btn');
	var $defaultText=$this.text();
    var $this=$('#add_new_btn');
	var $defaultText=$this.text();
	var requestcallbackurl=BASE_URL+'api/products';
	var form=$('#add_product_form')[0];
	var formData=new FormData();
    formData.append('product_name',$('#product_name').val());
    formData.append('product_size',$('#product_size').val());
    formData.append('quantity',$('#quantity').val());
    formData.append('product_option',$('#product_option').val());
    formData.append('vendor_price',$('#vendor_price').val());
    formData.append('product_price',$('#product_price').val());
    formData.append('product_weight',$('#product_weight').val());
    formData.append('product_days',$('#product_days').val());
    formData.append('product_sku',$('#product_sku').val());
    formData.append('artwork_name',$('#artwork_name').val());
    formData.append('status',$('#status').val());

    if(document.getElementById('product_image') != undefined && document.getElementById('product_image').files != undefined && document.getElementById('product_image').files[0] != undefined){
    	formData.append('product_image',document.getElementById('product_image').files[0]);
    }
	

    if($('#add_product_form').valid()){
        $this.addClass('disable');
        $this.attr('disable',true);
        $this.text('Please Wait...');

        $http({
            method  : 'POST',
            url     : requestcallbackurl,
            data    : formData, //forms user object
            headers : {'Content-Type':undefined}

        }).then(function(response) {
            $scope.data =response.data.message;

            alert($scope.data);
            $window.location.href = BASE_URL+'product';
        });
    }

};


$scope.editProduct = function(ev) {
    
    var $this=$('#edit_new_btn');
    var $defaultText=$this.text();
    var requestcallbackurl=BASE_URL+'api/product-update';
    
    var frm = $('#edit_product_form');
    var formData = new FormData(frm[0]);
    formData.append('product_image', $('input[type=file]')[0].files[0]);
    
    if($('#edit_product_form').valid()){
        $this.addClass('disable');
        $this.attr('disable',true);
        $this.text('Please Wait...');

        $http({
            method  : 'POST',
            url     : requestcallbackurl,
            data    : formData, //forms user object
            headers : {'Content-Type':undefined}

        }).then(function(response) {
            $this.removeClass('disable');
            $this.removeAttr('disabled');
            $this.text('Submit');
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('body')))
                .clickOutsideToClose(false)
                .title('')
                .textContent(response.data.messege)
                .ariaLabel('')
                .ok('Ok')
                .targetEvent(ev)
            ).finally(function() {
                if(response.data.status != false){
                    $window.location.href = BASE_URL+'product';
                }
                
            });

           
                
        });
    }
};

});


