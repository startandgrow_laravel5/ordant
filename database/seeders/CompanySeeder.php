<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::truncate();

        $company = [
            [
                'country' => 1,
                'name' => 'test company1.co',
                'email' => 'test1@gmail.com',
                'website' => 'www.websitetest.com',
                'linkedin_profile' => 'www.linkedin.com',
                'facebook_url' => 'www.fb.com',
                'head_office_number' => '00000-00000',
            ],
            [
                'country' => 1,
                'name' => 'test company2.co',
                'email' => 'test2@gmail.com',
                'website' => 'www.websitetest1.com',
                'linkedin_profile' => 'www.linkedin.com',
                'facebook_url' => 'www.fb.com',
                'head_office_number' => '00000-00000',
            ],
        ];
        Company::insert($company); 
       
    }
}
