<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        
        $users =  [
            ['fname' => 'Admin', 'lname' => 'Admin', 'username' => 'Admin', 'email' => 'admin@gmail.com', 'password' =>  bcrypt('root'), 'disable_account' => '0', 'sales_person_id' => '0', 'contact_number' => '9999999999', 'country_id' => '1', 'region_id' => '1', 'town_id' => '1', 'address_line_1' => 'Lorem Ipsum', 'postcode' => '400001'],
            ['fname' => 'Sales Person1', 'lname' => 'Sales', 'username' => 'sales_person1', 'email' => 'sales1@gmail.com', 'password' =>  bcrypt('root'), 'disable_account' => '0', 'sales_person_id' => '1', 'contact_number' => '9999999998', 'country_id' => '1', 'region_id' => '1', 'town_id' => '1', 'address_line_1' => 'Lorem Ipsum', 'postcode' => '400001'],
            ['fname' => 'Sales Person2', 'lname' => 'Sales', 'username' => 'sales_person2', 'email' => 'sales2@gmail.com', 'password' =>  bcrypt('root'), 'disable_account' => '0', 'sales_person_id' => '1', 'contact_number' => '9999999997', 'country_id' => '1', 'region_id' => '1', 'town_id' => '1', 'address_line_1' => 'Lorem Ipsum', 'postcode' => '400001']
          ];
          User::insert($users);
    }
}
