<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserRole;
class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserRole::truncate();

        $user_role =  [
            ['user_id' => '2', 'role_id' => '3'],
            ['user_id' => '3', 'role_id' => '3'],
          ];
          UserRole::insert($user_role);
    }
}
