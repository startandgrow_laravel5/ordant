<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Contact;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::truncate();

        $contacts = [
            [
                'country' => 1,
                'name' => 'contact1',
                'email' => 'contact1@gmail.com',
                'website' => 'www.contact1.com',
                'linkedin_profile' => 'www.linkedin.com',
                'facebook_url' => 'www.fb.com',
                'head_office_number' => '00000-00000',
            ],
            [
                'country' => 1,
                'name' => 'contact2',
                'email' => 'contact2@gmail.com',
                'website' => 'www.contact1.com',
                'linkedin_profile' => 'www.linkedin.com',
                'facebook_url' => 'www.fb.com',
                'head_office_number' => '00000-00000',
            ],
        ];
        Contact::insert($contacts); 
       
    }
}
