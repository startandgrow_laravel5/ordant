<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SiteSetting;
class SiteSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteSetting::truncate();

        $SiteSetting =  [
            ['identifier' => 'customerCategory', 'name' => 'Category', 'type' => 'select'],
            ['identifier' => 'customerRateCode', 'name' => 'Rate Code', 'type' => 'select'],
            ['identifier' => 'customerRelation', 'name' => 'Customer Relation', 'type' => 'select'],
            ['identifier' => 'defaultDecimal', 'name' => 'Decimal Places', 'type' => 'value'],
            ['identifier' => 'identityKey', 'name' => 'Identity Key', 'type' => 'identityKey'],
            ['identifier' => 'paymentTerms', 'name' => 'Payment Terms', 'type' => 'select'],
            ['identifier' => 'priority', 'name' => 'Priority', 'type' => 'select'],
            ['identifier' => 'shipmentService', 'name' => 'Shipment Service', 'type' => 'select'],
            ['identifier' => 'states', 'name' => 'US States', 'type' => 'select'],
            ['identifier' => 'status', 'name' => 'Status', 'type' => 'select'],
            ['identifier' => 'taxRate', 'name' => 'Tax Rate', 'type' => 'value'],
            ['identifier' => 'timeZone', 'name' => 'Default Time Zone', 'type' => 'setValue']
          ];
          SiteSetting::insert($SiteSetting);
    }
}
