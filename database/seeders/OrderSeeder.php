<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order')->truncate();
        $orders = [
            [
                'est_id' => '1',
                'cust_id' => '1',
                'supl_id' => '1',
                'order_no' => '1000001',
                'order_date' => '2021-12-14',
                'quote_id' =>2,
                //'price' => '500',
                //'qty' => '2',
                'total' => '1000',
                //'status' => '1',
            ],
            [
                'est_id' => '1',
                'cust_id' => '1',
                'supl_id' => '1',
                'order_no' => '1000002',
                'order_date' => '2021-12-13',
                'quote_id' =>2,
                //'price' => '1000',
                //'qty' => '1',
                'total' => '1000',
                //'status' => '0',
            ],
        ];
        DB::table('order')->insert($orders);
    }
}
