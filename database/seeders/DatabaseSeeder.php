<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminUserSeeder::class);

        $this->call(SiteSettingSeeder::class);

        $this->call(CompanySeeder::class);

        $this->call(EstimateSeeder::class);

        $this->call(ContactSeeder::class);

        $this->call(RolesSeeder::class);
        
        $this->call(UserRoleSeeder::class);

        $this->call(CountriesSeeder::class);

        $this->call(OrderSeeder::class);
    }
}
