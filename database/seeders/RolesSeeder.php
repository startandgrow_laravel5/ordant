<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OmsRoles;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OmsRoles::truncate();
        
        $roles =  [
            ['name' => 'Superadmin', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Master Admin', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Sales Person', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Master Company', 'is_crm_user' => '0', 'status' => '1', 'order' => '1'],
            ['name' => 'Sub Company', 'is_crm_user' => '0', 'status' => '1', 'order' => '1'],
            ['name' => 'Learner', 'is_crm_user' => '0', 'status' => '1', 'order' => '1'],
            ['name' => 'Support Agents', 'is_crm_user' => '0', 'status' => '1', 'order' => '1'],
            ['name' => 'Sub Master', 'is_crm_user' => '0', 'status' => '1', 'order' => '1'],
            ['name' => 'Individual', 'is_crm_user' => '0', 'status' => '1', 'order' => '1'],
            ['name' => 'Sales Manager', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Trainer', 'is_crm_user' => '0', 'status' => '1', 'order' => '1'],
            ['name' => 'Administrator', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Finance Manager', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Bookings Manager', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Tech Manager', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Office Manager', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Recruitment', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Social Media Manager', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'General Admin', 'is_crm_user' => '1', 'status' => '1', 'order' => '1'],
            ['name' => 'Third Party', 'is_crm_user' => '0', 'status' => '1', 'order' => '1'],
            ['name' => 'Development CRM', 'is_crm_user' => '1', 'status' => '1', 'order' => '1']
          ];
          OmsRoles::insert($roles);
    }
}
