<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Estimate;

class EstimateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estimate::truncate();

        $estimates =  [
            ['project_name' => 'Demo Project', 'company_id' => '1', 'contact_id' => '1', 'po_number' => '10001', 'notes' => 'Demo', 'qty' => '5', 'total' => '10000'],
            ['project_name' => 'Demo Project2', 'company_id' => '1', 'contact_id' => '1', 'po_number' => '10002', 'notes' => 'Demo2', 'qty' => '10', 'total' => '20000'],
          ];
          Estimate::insert($estimates);
    }
}
