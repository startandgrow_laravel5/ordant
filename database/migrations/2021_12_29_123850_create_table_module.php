<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_module', function (Blueprint $table) {
            $table->id();
            $table->string('module_name',191);
            $table->string('module_slug',191);
            $table->integer('sorder')->nullable()->default('0');
            $table->integer('parent_module')->nullable()->default('0');
            $table->enum('include_add_new', ['0', '1'])->default('0')->nullable();
            $table->integer('total_count')->default('0')->comment('No. of records in this module');
            $table->enum('related_with', ['0', '1', '2'])->default('0')->comment('0=nothing,1=company,2=contact');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_module');
    }
}
