<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->integer('country');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('website')->nullable();
            $table->string('linkedin_profile')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('head_office_number')->nullable();
            $table->string('secondary_contact_number')->nullable();
            $table->integer('sales_person')->nullable();
            $table->boolean('status')->comment('(0=active,1=deactive')->default(0);
            $table->integer('created_by')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact');
    }
}
