<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanySubContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_sub_contact', function (Blueprint $table) {
            $table->id();
            $table->integer('sales_person_id');
            $table->integer('company_id');
            $table->string('contact_first_name');
            $table->string('contact_last_name');
            $table->string('contact_email')->unique();
            $table->string('contact_mobile_number');
            $table->string('extension1')->nullable();
            $table->string('contact_landline1')->nullable();
            $table->string('extension2')->nullable();
            $table->string('contact_landline2')->nullable();
            $table->string('linkedin_profile')->nullable();
            $table->integer('country')->nullable();
            $table->string('location')->nullable();
            $table->string('city')->nullable();
            $table->string('secondary_contact_number')->nullable();
            $table->enum('online_training_service', ['Y', 'N'])->default('N')->nullable();
            $table->text('notes')->nullable();
            $table->string('contact_type')->default('sales')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_sub_contact');
    }
}
