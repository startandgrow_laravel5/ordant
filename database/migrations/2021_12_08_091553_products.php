<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            $table->string('product_size');
            $table->double('quantity');
            $table->string('product_option')->nullable();
            $table->double('vendor_price');
            $table->double('product_price');
            $table->double('product_weight')->nullable();
            $table->double('product_days')->nullable();
            $table->double('product_sku')->nullable();
            $table->string('artwork_name');
            $table->string('product_image')->nullable();
            $table->enum('status', ['0', '1'])->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
