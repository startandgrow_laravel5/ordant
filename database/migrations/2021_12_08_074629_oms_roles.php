<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OmsRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oms_roles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('is_crm_user')->comment('(1=CRM User, 0=Not CRM User')->default (0);
            $table->enum('status', ['0', '1'])->default('1');
            $table->integer('order')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
