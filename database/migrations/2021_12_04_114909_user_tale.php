<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserTale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('fname');
            $table->string('lname');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('disable_account')->comment('(0=enable,1=disable')->default(0);
            $table->integer('sales_person_id')->nullable();
            $table->string('contact_number');
            $table->string('secondary_contact_number')->nullable();
            $table->integer('is_permission')->nullable();
            $table->integer('is_default_sales_person')->nullable();
            $table->integer('country_id');
            $table->integer('region_id');
            $table->integer('town_id');
            $table->integer('company_account')->nullable();
            $table->integer('company_sub_account')->nullable();
            $table->string('signup_from')->nullable();
            $table->timestamp('last_login_at')->nullable();
            $table->string('last_login_ip')->nullable();
            $table->string('address_line_1');
            $table->string('address_line_2')->nullable();
            $table->string('postcode');
            $table->string('landline')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
