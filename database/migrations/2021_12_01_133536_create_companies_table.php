<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('companies', function (Blueprint $table) {
        //     $table->id();
        //     $table->string('name');
        //     $table->unsignedBigInteger('user_id');
        //     $table->foreign('user_id')->references('id')->on('users');
        //     $table->string('primary_phone')->nullable();
        //     $table->string('secondary_phone')->nullable();
        //     $table->string('fax')->nullable();
        //     $table->string('email')->nullable();
        //     $table->string('website')->nullable();
        //     $table->string('salesperson')->nullable();
        //     $table->string('accountmanager')->nullable();
        //     $table->string('city')->nullable();
        //     $table->string('state')->nullable();
        //     $table->string('billing_address')->nullable();
        //     $table->string('billing_street')->nullable();
        //     $table->string('billing_city')->nullable();
        //     $table->string('billing_pincode')->nullable();
        //     $table->string('shipping_address')->nullable();
        //     $table->string('shipping_street')->nullable();
        //     $table->string('shipping_city')->nullable();
        //     $table->string('shipping_pincode')->nullable();
        //     $table->string('notes')->nullable();
        //     $table->boolean('status')->default(1);
        //     $table->string('account_manager')->nullable();
        //     $table->string('customerrelationship_id')->nullable();
        //     $table->string('tax_id')->nullable();
        //     $table->string('paymentterms_id')->nullable();
        //     $table->string('rate_code')->nullable();
        //     $table->string('reseller_id')->nullable();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('companies');
    }
}
