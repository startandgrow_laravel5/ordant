<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableEstimate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimates', function (Blueprint $table) {
            $table->id();
            $table->string('project_name'); 
            $table->integer('company_id'); 
            $table->integer('contact_id');
            $table->string('po_number');
            $table->string('notes');
            $table->double('qty');
            $table->double('discount')->nullable();
            $table->double('discount_per')->nullable();
            $table->double('tax')->nullable();
            $table->double('tax_per')->nullable();
            $table->double('total');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
