<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SiteSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('site_setting')) {
            Schema::create('site_setting', function (Blueprint $table) {
                $table->increments('id');
                $table->string('identifier');
                $table->string('name');
                $table->string('type');
                $table->integer('updated_by')->nullable();
                $table->integer('created_by')->nullable();
                $table->timestamps();
            });
            }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
