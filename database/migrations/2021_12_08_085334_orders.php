<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('est_id'); 
            $table->integer('cust_id'); 
            $table->integer('supl_id');
            $table->string('order_no');
            $table->date('order_date');
            $table->double('price');
            $table->string('qty');
            $table->double('discount')->nullable();
            $table->double('discount_per')->nullable();
            $table->double('tax')->nullable();
            $table->double('tax_per')->nullable();
            $table->double('total');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
