@extends('layouts.master')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Site Setting</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Sr No.</th>
                    <th>Identifier</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Value Count</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @php $i = 1; @endphp
                  @foreach($sitesetting as $data)
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $data->identifier }}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->type }}</td>
                    <td>{{ $data->type }}</td>
                    <td class="text-center">
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                    <i class="fas fa-edit"></i>
                      </button></td>
                  </tr>
                  @php $i++; @endphp
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- modal -->
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Select Setting</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="col-md-10" style="margin-left: 5px">
        <div class="table-responsive">
          <table id="test-table" class="table table-condensed">
            <thead>
              <tr>
                <th style="border-top: 0px!important;border-bottom: 0px!important;">From</th>
                <th style="border-top: 0px!important;border-bottom: 0px!important;">To</th>
              </tr>
            </thead>
            <tbody id="test-body">
              <tr id="row0">
                <td style="border-top: 0px!important;border-bottom: 0px!important;">
                  <input name='from_value0' value='100' type='text' class='form-control' />
                </td>
                <td style="border-top: 0px!important;border-bottom: 0px!important;">
                  <input name='to_value0' value='500' type='text' class='form-control input-md' />
                </td>
                <td style="border-top: 0px!important;border-bottom: 0px!important;">
                  <input class='delete-row btn btn-primary' type='button' value='Delete' />
                </td>
              </tr>
            </tbody>
          </table>
          <input style="margin-bottom: 10px;
    margin-left: 10px;" id='add-row' class='btn btn-primary' type='button' value='Add' />
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- modal -->

@endsection