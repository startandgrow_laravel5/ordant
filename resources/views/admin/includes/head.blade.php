<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--{{ config('app.name', 'Laravel') }}-->
<title>OMS</title>

<!-- Scripts -->


<!-- Fonts -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/developer.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/material-dashboard.min.css?v=2.1.2') }}" rel="stylesheet" />

<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="{{ asset('admin/demo/demo.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
<!-- script file for javascript -->
<script type="text/javascript" src="{{ asset('admin/js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/jquery-ui.min.js') }}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.0/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.0/angular-animate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.0/angular-aria.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.0/angular-messages.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.8/angular-sanitize.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular-resource.min.js"></script>


<script src="{{ asset('admin/js/ui-bootstrap-tpls-0.12.0.js') }}"></script>
<script src="{{ asset('admin/js/controller/MainController.js') }}"></script>
<script src="{{ asset('admin/js/controller/MainController.js') }}"></script>
