    <div class="sidebar" data-color="rose" data-background-color="black" data-image="{{ asset('admin/img/sidebar-1.jpg') }}">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="{{ url('home') }}" class="simple-text logo-mini">
        OM
        </a>
        <a href="{{ url('home') }}" class="simple-text logo-normal">
          O-Management
        </a></div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="{{ asset('admin/img/user.png') }}" />
            
          </div>
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username">
              <span>
               {{Auth::user()->fname}}
                <!-- {{Auth::user()->name}} -->
                <b class="caret"></b>
              </span>
            </a>
             <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> MP </span>
                    <span class="sidebar-normal"> My Profile </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> EP </span>
                    <span class="sidebar-normal"> Edit Profile </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> S </span>
                    <span class="sidebar-normal"> Settings </span>
                  </a>
                </li>
              </ul>
            </div> 
          </div>
        </div>
        <ul class="nav">
          <li class="nav-item {{ Request::segment(1) === 'home' ? 'active' : null }} ">
            <a class="nav-link" href="{{ url('home') }}">
              <i class="material-icons">dashboard</i>
              <p> Dashboard </p>
            </a>
          </li>
           {{-- <li class="nav-item ">
            <a class="nav-link" href="{{ url('/admin/product') }}"> 
              <i class="material-icons">accessibility</i>
              <p> Product </p>
            </a>
          </li> --}}
          <li class="nav-item 
          {{ Request::segment(1) === 'companies' ? 'active' : null }}
          {{ Request::segment(1) === 'contact' ? 'active' : null }}
          ">
            <a class="nav-link" data-toggle="collapse" href="#tablesExamples1">
              <i class="material-icons">accessibility</i>
              <p> Customers
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse 
            {{ Request::segment(1) === 'companies' ? 'show' : null }}
          {{ Request::segment(1) === 'contact' ? 'show' : null }}
            " id="tablesExamples1">
              <ul class="nav">
            
             
                <li class="nav-item 
                {{ Request::segment(1) === 'companies' ? 'active' : null }}
                ">
                  <a class="nav-link" href="{{route('companies.index')}}">
                    <span class="sidebar-mini"> CL</span>
                    <span class="sidebar-normal"> Company List  </span>
                  </a>
                </li>
                <li class="nav-item
                {{ Request::segment(1) === 'contact' ? 'active' : null }}
                ">
                  <a class="nav-link" href="{{route('contact.index')}}">
                    <span class="sidebar-mini"> CL </span>
                    <span class="sidebar-normal">Contact List</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item 
          {{ Request::segment(1) === 'product' ? 'active' : null }}
          {{ Request::segment(1) === 'quotes' ? 'active' : null }}
          {{ Request::segment(1) === 'orders' ? 'active' : null }}">
            <a class="nav-link" data-toggle="collapse" href="#tablesExamples">
              <i class="material-icons">grid_on</i>
              <p> Projects
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse 
            {{ Request::segment(1) === 'product' ? 'show' : null }}
          {{ Request::segment(1) === 'quotes' ? 'show' : null }}
          {{ Request::segment(1) === 'orders' ? 'show' : null }}
            " id="tablesExamples">
              <ul class="nav">
                <li class="nav-item 
                {{ Request::segment(1) === 'product' ? 'active' : null }}
                ">
                  <a class="nav-link" href="{{route('product.index')}}">
                    <span class="sidebar-mini"> PL </span>
                    <span class="sidebar-normal"> Product List </span>
                  </a>
                </li>
                <li class="nav-item 
                {{ Request::segment(1) === 'quotes' ? 'active' : null }}
                ">
                  <a class="nav-link" href="{{route('quotes.index')}}">
                    <span class="sidebar-mini"> QL </span>
                    <span class="sidebar-normal">Quote List </span>
                  </a>
                </li> 
                <li class="nav-item 
                {{ Request::segment(1) === 'orders' ? 'active' : null }}
                ">
                  <a class="nav-link" href="{{route('orders.index')}}">
                    <span class="sidebar-mini"> OL </span>
                    <span class="sidebar-normal"> Orders </span>
                  </a>
                </li>
         
                {{-- <li class="nav-item ">
                  <a class="nav-link" href="">
                    <span class="sidebar-mini"> OM</span>
                    <span class="sidebar-normal"> Order Manager</span>
                  </a>
                </li> --}}
              </ul>
            </div>
          </li>
          <li class="nav-item {{ Request::segment(1) === 'email-template' ? 'active' : null }}">
            <a class="nav-link" href="{{route('email-template.index')}}"> 
              <i class="material-icons">email</i>
              <p> Email Template</p>
            </a>
          </li>
          
          {{-- <li class="nav-item ">
            <a class="nav-link" href=""> 
              <i class="material-icons">accessibility</i>
              <p> Reports</p>
            </a>
          </li> --}}
          <li class="nav-item ">
            <a class="nav-link" href=""> 
              <i class="material-icons">add_ic_call</i>
              <p> Admin</p>
            </a>
          </li>
          
           <li class="nav-item ">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> 
              <i class="material-icons">login</i>
              <p> Logout </p>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
            </a>
          </li>
        </ul>
      </div>
    </div>