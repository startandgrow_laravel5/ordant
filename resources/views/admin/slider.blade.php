@extends('admin.layouts.app')

@section('content')
<div class="container-fluid" ng-controller="DashboardController">
    <div class="row">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Add Slider Image</h4>
                </div>
            </div>
            <div class="card-body ">
                <form id="add_slider_form" ng-submit="addSliderImage()" accept-charset="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-sm-6 input-field">
                            <label>Name</label>
                            <div class="form-group">
                                <input id="name" class="form-control" name="name" required="true">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label>Showing On </label>
                            <div class="form-group select-wizard">
                              <select class="selectpicker" name="type" data-style="select-with-transition" title="Single Select" >
                                <option value="Afghanistan">Web </option>
                                <option value="Albania"> Android App</option>
                                <option value="Algeria"> Ios App</option>
                              </select>
                            </div>
                          </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <h4 class="title">Add Slider Image</h4>
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                    <img src="{{ asset('admin/img/image_placeholder.jpg') }}" alt="Add Slider">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                <div>
                                    <span class="btn btn-rose btn-round btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="upload_slider" required="true">
                                    </span>
                                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="card-footer">
                            
                             <button class="btn btn-rose" type="submit" id="add_new_btn" name="action">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>     
    </div>
</div>
@endsection

@section('jsScript')
 <script>
    function setFormValidation(id) {
      $(id).validate({
        highlight: function(element) {
          $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
          $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
        },
        success: function(element) {
          $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
          $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
        },
        errorPlacement: function(error, element) {
          $(element).closest('.form-group').append(error);
        },
      });
    }

    $(document).ready(function() {
      setFormValidation('#add_slider_form');
    });
  </script>
  <script src="{{ asset('admin/js/controller/DashboardController.js') }}"></script>
  
@endsection