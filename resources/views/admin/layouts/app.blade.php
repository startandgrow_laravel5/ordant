<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Head Part -->
    @include('admin.includes.head') 
    <!-- Head Part  -->

    @include('admin.includes.js-constant-variable')

</head>

<body class="" ng-app="OrdantApp" data-simplebar>
    <!-- Loading Overlay for global use -->
    <div class="loading hide" id="loading_bar">
        <span><!-- <img src="{{ asset('admin/img/loader.gif') }}" height="100px" width="100px" > --></span>
    </div>
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!-- Sidebar -->
    @include('admin.includes.sidebar')

    <div class="main-panel">
      <!-- Navbar -->
      @include('admin.includes.header')
      <!-- End Navbar -->
      <div class="content">
        <!-- START CONTENT -->
        @yield('content')
      </div>

      <!-- START FOOTER -->
      @include('admin.includes.footer')

</body>

</html>