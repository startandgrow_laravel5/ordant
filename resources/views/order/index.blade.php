@extends('admin.layouts.app')

@section('content')
<style type="text/css">
  .img-container.icon_img {
    width: 50%;
  }
</style>

<div id="preloaders" class="preloader"></div>

<div class="container-fluid" ng-controller="OrderController">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-rose card-header-icon">
          <div class="card-icon">
            <i class="material-icons">assignment</i>
          </div>
          <h4 class="card-title">Order List</h4>
          <div style="float: right;">
            <a class="btn btn-rose" href="{{ route('order_pdf') }}">Export Orders PDF</a>
            <a class="btn btn-rose" href="{{ route('order_csv') }}">Export Orders CSV</a>
          </div>
          {{-- <div class="col-md-2 pull-right">
                  <a class="btn bg-gradient-primary mb-0 mt-lg-auto w-100" href="{{ route('companies.create') }}">Add Order</a>
        </div> --}}
        <div class="col-md-2 pull-right">
          <div class="input-group">

            <input type="text" class="form-control" placeholder="Search Here" ng-model='search'>
          </div>

        </div>

      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table responsive-table mainTable" ng-init="getorderList()">
            <thead>
              <tr>
                {{-- <th>Icon</th> --}}
                <th>Order No</th>
                <th>Supplier Name</th>
                <th>Customer name</th>
                <th>Total</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="horscope in orderList | filter:search" ng-class="contacts.tr_class">
                <td>ORN<@ horscope.order_no @>
                </td>
                <td>
                  <@ horscope.supplier_name @>
                </td>
                <td>
                  <@ horscope.customer_name @>
                </td>
                <td>
                  <@ horscope.total @>
                </td>
                <td>
                  <button type="button" class="btn btn-info" ng-show="horscope.status==1">Accepted</button>

                </td>
                <td class="td-actions text-right">
                  <a href="{{url('quotes/<@ horscope.id @>/edit')}}" type="button" rel="tooltip" class="btn btn-success">
                    <i class="material-icons">edit</i>
                  </a>

                </td>
              </tr>

            </tbody>
          </table>
          <div ng-if="orderList.length==0">
            <p class="norecord norecord-border mR10">
              <img src="{{ asset('admin/img/nodatafound.svg')}}">
              <span>No Records Found.</span>
            </p>
          </div>
          <div class="pagination" ng-show="orderList  ">
            <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageContactChanged()" items-per-page="numPerPage" boundary-links="true" rotate="false" class="pagination-sm" max-size="maxSize"></pagination>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('jsScript')
<script>
  (function($) {
    var preloader = $(".preloader");
    setTimeout(function() {
      preloader.remove();
    }, 2000);
  })(jQuery);
</script>

<script src="{{ asset('admin/js/controller/OrderController.js') }}"></script>

@endsection