
<div class="sidebar" data-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg">
  <!--
    Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

    Tip 2: you can also add an image using data-image tag
-->
  <div class="logo"><a href="{{ route('home') }}" class="simple-text logo-mini">
      OMS
    </a>
    <a href="{{ route('home') }}" class="simple-text logo-normal">
      Order Management System
    </a></div>
  <div class="sidebar-wrapper">
    <div class="user">
      <div class="photo">
        <img src="../assets/img/faces/avatar.jpg" />

<nav class="main-header navbar navbar-expand-md navbar-light navbar-white" style="background-color:#fff !important;">
    <div class="container">
      <a href="{{ route('home') }}" class="navbar-brand">
        <img src="{{ asset('assets/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">Home</a>
          </li>
         
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Customers</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="{{route('companies.index')}}" class="dropdown-item">Company List </a></li>
              <li><a href="#" class="dropdown-item">Contact List</a></li>

            </ul>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Projects</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="#" class="dropdown-item">Order List</a></li>
              <li><a href="#" class="dropdown-item">Estimate List </a></li>
              {{-- <li><a href="#" class="dropdown-item">Order Manager</a></li> --}}

              <!-- End Level two -->
            </ul>
          </li>
          {{-- <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Catalog</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="#" class="dropdown-item">Order List</a></li>
              <li><a href="#" class="dropdown-item">Estimate List </a></li>
              <li><a href="#" class="dropdown-item">Order Manager</a></li>

              <!-- End Level two -->
            </ul>
          </li> --}}
          {{-- <li class="nav-item">
            <a href="index3.html" class="nav-link">Reports</a>
          </li> --}}

          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Admin</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="#" class="dropdown-item">My Company</a></li>
              <li><a href="{{ route('site_setting') }}" class="dropdown-item">Site Setting </a></li>

              <!-- End Level two -->
            </ul>
          </li>
        </ul>

        <!-- SEARCH FORM -->
        {{-- <form class="form-inline ml-0 ml-md-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form> --}}

      </div>
      <div class="user-info">
        <a data-toggle="collapse" href="#collapseExample" class="username">
          <span>
            Tania Andrew
            <b class="caret"></b>
          </span>
        </a>
        <div class="collapse" id="collapseExample">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span class="sidebar-mini"> MP </span>
                <span class="sidebar-normal"> My Profile </span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span class="sidebar-mini"> EP </span>
                <span class="sidebar-normal"> Edit Profile </span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span class="sidebar-mini"> S </span>
                <span class="sidebar-normal"> Settings </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <ul class="nav">
      <li class="nav-item active ">
        <a class="nav-link" href="../examples/dashboard.html">
          <i class="material-icons">dashboard</i>
          <p> Dashboard </p>
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#pagesExamples">
          <i class="material-icons">image</i>
          <p> Pages
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="pagesExamples">
          <ul class="nav">
            <li class="nav-item ">
              <a class="nav-link" href="../examples/pages/pricing.html">
                <span class="sidebar-mini"> P </span>
                <span class="sidebar-normal"> Pricing </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/pages/rtl.html">
                <span class="sidebar-mini"> RS </span>
                <span class="sidebar-normal"> RTL Support </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/pages/timeline.html">
                <span class="sidebar-mini"> T </span>
                <span class="sidebar-normal"> Timeline </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/pages/login.html">
                <span class="sidebar-mini"> LP </span>
                <span class="sidebar-normal"> Login Page </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/pages/register.html">
                <span class="sidebar-mini"> RP </span>
                <span class="sidebar-normal"> Register Page </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/pages/lock.html">
                <span class="sidebar-mini"> LSP </span>
                <span class="sidebar-normal"> Lock Screen Page </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/pages/user.html">
                <span class="sidebar-mini"> UP </span>
                <span class="sidebar-normal"> User Profile </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/pages/error.html">
                <span class="sidebar-mini"> E </span>
                <span class="sidebar-normal"> Error Page </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#componentsExamples">
          <i class="material-icons">apps</i>
          <p> Components
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="componentsExamples">
          <ul class="nav">
            <li class="nav-item ">
              <a class="nav-link" data-toggle="collapse" href="#componentsCollapse">
                <span class="sidebar-mini"> MLT </span>
                <span class="sidebar-normal"> Multi Level Collapse
                  <b class="caret"></b>
                </span>
              </a>
              <div class="collapse" id="componentsCollapse">
                <ul class="nav">
                  <li class="nav-item ">
                    <a class="nav-link" href="#0">
                      <span class="sidebar-mini"> E </span>
                      <span class="sidebar-normal"> Example </span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/components/buttons.html">
                <span class="sidebar-mini"> B </span>
                <span class="sidebar-normal"> Buttons </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/components/grid.html">
                <span class="sidebar-mini"> GS </span>
                <span class="sidebar-normal"> Grid System </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/components/panels.html">
                <span class="sidebar-mini"> P </span>
                <span class="sidebar-normal"> Panels </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/components/sweet-alert.html">
                <span class="sidebar-mini"> SA </span>
                <span class="sidebar-normal"> Sweet Alert </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/components/notifications.html">
                <span class="sidebar-mini"> N </span>
                <span class="sidebar-normal"> Notifications </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/components/icons.html">
                <span class="sidebar-mini"> I </span>
                <span class="sidebar-normal"> Icons </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/components/typography.html">
                <span class="sidebar-mini"> T </span>
                <span class="sidebar-normal"> Typography </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#formsExamples">
          <i class="material-icons">content_paste</i>
          <p> Forms
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="formsExamples">
          <ul class="nav">
            <li class="nav-item ">
              <a class="nav-link" href="../examples/forms/regular.html">
                <span class="sidebar-mini"> RF </span>
                <span class="sidebar-normal"> Regular Forms </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/forms/extended.html">
                <span class="sidebar-mini"> EF </span>
                <span class="sidebar-normal"> Extended Forms </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/forms/validation.html">
                <span class="sidebar-mini"> VF </span>
                <span class="sidebar-normal"> Validation Forms </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/forms/wizard.html">
                <span class="sidebar-mini"> W </span>
                <span class="sidebar-normal"> Wizard </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#tablesExamples">
          <i class="material-icons">grid_on</i>
          <p> Tables
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="tablesExamples">
          <ul class="nav">
            <li class="nav-item ">
              <a class="nav-link" href="../examples/tables/regular.html">
                <span class="sidebar-mini"> RT </span>
                <span class="sidebar-normal"> Regular Tables </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/tables/extended.html">
                <span class="sidebar-mini"> ET </span>
                <span class="sidebar-normal"> Extended Tables </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/tables/datatables.net.html">
                <span class="sidebar-mini"> DT </span>
                <span class="sidebar-normal"> DataTables.net </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#mapsExamples">
          <i class="material-icons">place</i>
          <p> Maps
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="mapsExamples">
          <ul class="nav">
            <li class="nav-item ">
              <a class="nav-link" href="../examples/maps/google.html">
                <span class="sidebar-mini"> GM </span>
                <span class="sidebar-normal"> Google Maps </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/maps/fullscreen.html">
                <span class="sidebar-mini"> FSM </span>
                <span class="sidebar-normal"> Full Screen Map </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="../examples/maps/vector.html">
                <span class="sidebar-mini"> VM </span>
                <span class="sidebar-normal"> Vector Map </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="../examples/widgets.html">
          <i class="material-icons">widgets</i>
          <p> Widgets </p>
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="../examples/charts.html">
          <i class="material-icons">timeline</i>
          <p> Charts </p>
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="../examples/calendar.html">
          <i class="material-icons">date_range</i>
          <p> Calendar </p>
        </a>
      </li>
    </ul>
  </div>
</div>