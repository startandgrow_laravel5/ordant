@include('layouts.includes.header')
  <body class="hold-transition layout-top-nav">
      <div class="wrapper">
      
        <!-- Navbar -->
        @include('layouts.includes.nav')
        <!-- /.navbar -->
      
        @yield('content')
      
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
          <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
      
        <!-- Main Footer -->
      
@include('layouts.includes.footer')