@extends('admin.layouts.app')

@section('content')
<div class="container-fluid" ng-controller="EmailTemplateController">
    <div class="row">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Update Template</h4>
                </div>
            </div>
            <div class="card-body ">
                <form id="edit_template_form" ng-submit="editTemplate()" accept-charset="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-sm-6 input-field">
                            <label>Name <strong class="required-input">*</strong></label>
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$template->id}}">
                                <input id="name" data-rule-required="true" data-msg-required="Enter template name."  class="form-control" name="name" value="{{$template->name}}">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Template Type <strong class="required-input">*</strong></label>
                            <div class="form-group">
                                <select class="selectpicker" id="template_type" data-rule-required="true" data-msg-required="Please select template type." name="template_type" data-style="select-with-transition" title="Single Select" >
                                    <option value="new-registration-email" @if($template->template_type=='new-registration-email') selected @endif>New Registration Template</option>
                                    <option value="quote-send-email" @if($template->template_type=='quote-send-email') selected @endif>Quote Send Template</option>
                                  </select>
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Subject <strong class="required-input">*</strong></label>
                            <div class="form-group">
                                <input id="subject" class="form-control" name="subject" data-rule-required="true" data-msg-required="Enter template subject." value="{{$template->subject}}">
                            </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <label>Status </label>
                            <div class="form-group select-wizard">
                              <select class="selectpicker" data-rule-required="true" data-msg-required="Please select assigned to." id="status" name="status" data-style="select-with-transition" title="Single Select" >
                                <option value="1" @if($template->status=='1') selected @endif>Active </option>
                                <option value="0" @if($template->status=='0') selected @endif> In Active</option>
                              </select>
                            </div>
                        </div>
                        <div class="col-sm-12 input-field">
                            <label>Sms Content <strong>(Optional)</strong></label>
                            <div class="form-group">
                                <input id="sms_content" class="form-control" name="sms_content" value="{{$template->sms_content}}">
                            </div>
                        </div>
                        <div class="col-sm-12 input-field">
                            <label>Email Content <strong class="required-input">*</strong></label>
                            <div class="form-group">
                                <textarea id="content" data-rule-required="true" data-msg-required="Enter email body." >{{$template->content}}</textarea>
                            </div>
                        </div>
                       
                    </div>
               
             
                    <div class="row">
                        <div class="card-footer">
                            
                             <button class="btn btn-rose" type="submit" id="edit_new_btn" name="action">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>     
    </div>
</div>
@endsection

@section('jsScript')
 <script>
    $(document).ready(function() {
      CKEDITOR.replace('content');
    });
  </script>
  <script src="{{ asset('admin/js/controller/EmailTemplateController.js') }}"></script>
 <script src="{{ asset('admin/js/ckeditor/ckeditor/ckeditor.js') }}"></script>
  <script>
        
    </script>
@endsection