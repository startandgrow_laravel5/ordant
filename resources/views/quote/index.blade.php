@extends('admin.layouts.app') @section('content')
<style type="text/css">
    .img-container.icon_img {
        width: 50%;
    }
</style>
<div id="preloaders" class="preloader"></div>

<div class="container-fluid" ng-controller="QuoteController">
    <input type="hidden" id="currentuserId" name="currentuserId" value="{{Auth::user()->id}}">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">assignment</i>
                    </div>
                    <h4 class="card-title">Quote List</h4>
                    <div style="float: right;">
                        <a class="btn btn-rose" href="{{ route('quote_pdf') }}">Export Quotes PDF</a>
                        <a class="btn btn-rose" href="{{ route('quote_csv') }}">Export Quotes CSV</a>
                    </div>
                    <div class="col-md-2 pull-right">
                        <a class="btn bg-gradient-primary mb-0 mt-lg-auto w-100" href="{{ route('quotes.create') }}">Add Quote</a>
                    </div>
                    <div class="col-md-2 pull-right">
                        <div class="input-group">

                            <input type="text" class="form-control" placeholder="Search Here" ng-model='search'>
                        </div>

                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table responsive-table mainTable" ng-init="getquoteList()">
                            <thead>
                                <tr>
                                    {{--
                                    <th>Icon</th>
                                    --}}
                                    <th>Ouote No</th>
                                    <th>Supplier Name</th>
                                    <th>Customer name</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="horscope in horscopeList | filter:search" ng-class="contacts.tr_class">
                                    <td>ORN<@ horscope.order_no @>
                                    </td>
                                    <td>
                                        <@ horscope.supplier_name @>
                                    </td>
                                    <td>
                                        <@ horscope.customer_name @>
                                    </td>
                                    <td>
                                        <@ horscope.total @>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-info" ng-show="horscope.status==1">Accepted</button>
                                        <button type="button" class="btn btn-success" ng-show="horscope.status==0">Pending</button>
                                        <button type="button" class="btn btn-warning" ng-show="horscope.status==2">Revision</button>
                                        <button type="button" class="btn btn-danger" ng-show="horscope.status==3">Canceld</button>
                                    </td>
                                    <td class="td-actions text-right">
                                        <a ng-click="getComment(horscope.id)" type="button" href="#" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" rel="tooltip" class="btn btn-success">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <!-- <a href="{{url('companies/<@ horscope.id @>/edit')}}" type="button" rel="tooltip" class="btn btn-success">
                                            <i class="material-icons">edit</i>
                                        </a> -->
                                    </td>
                                </tr>
                                <tr ng-if="horscopeList.length==0">
                                    <td colspan="15">No Records Found.</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Model -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Quote ORN<@ quotedetails.order_no @>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="add_comment_form" ng-submit="changeStatus()" accept-charset="multipart/form-data" method="post">
                                            <div class="col-sm-6 input-field">
                                                <label>Quote Option</label>
                                                <div class="form-group">
                                                    <select id="status" name="status" class="selectpicker" data-style="select-with-transition" title="Single Select">
                                                        <option value="2">Rivision</option>
                                                        <option value="1"> Accept</option>
                                                        <option value="3"> Canceled </option>
                                                        <option value="0"> Pending</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 input-field">
                                                <label class="col-form-label">comment :</label>
                                                <textarea class="form-control" id="comments" name="comments"></textarea>
                                            </div>
                                            <input type="hidden" name="customer_id" id="customer_id" value="{{$user = Auth::user()->id}}">
                                            <input type="hidden" name="order_id" id="order_id" value="<@ quotedetails.id @>">
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-rose" type="submit" id="add_new_btn" name="action">Submit</button>
                                            </div>
                                        </form>
                                        <div class="container">
                                            <div class="row d-flex justify-content-center">
                                                <div class="col-md-12">

                                                    <div class="card p-3" ng-repeat="quotecomment in quotecomments">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <div class="user d-flex flex-row align-items-center">
                                                                <img src="https://demos.creative-tim.com/material-dashboard-pro/assets/img/marie.jpg" width="30" class="user-img rounded-circle mr-2">
                                                                <span><small class="font-weight-bold text-primary">james_olesenn</small>
                                                                    <small class="font-weight-bold">
                                                                        <@ quotecomment.comments @>
                                                                    </small></span>
                                                            </div> <small>
                                                                <@ quotecomment.created_at @>
                                                            </small>
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- End Model -->
                        <div class="pagination" ng-show="horscopeList">
                            <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageContactChanged()" items-per-page="numPerPage" boundary-links="true" rotate="false" class="pagination-sm" max-size="maxSize"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection @section('jsScript')
<script>
    (function($) {
        var preloader = $(".preloader");
        setTimeout(function() {
            preloader.remove();
        }, 2000);
    })(jQuery);
</script>

<script>
    function openPage(pageName, elmnt, color) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById(pageName).style.display = "block";
        elmnt.style.backgroundColor = color;
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>
<script src="{{ asset('admin/js/controller/QuoteController.js') }}"></script>

@endsection