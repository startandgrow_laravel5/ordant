@extends('admin.layouts.app')

@section('content')
    
<div id="preloaders" class="preloader"></div>
<div class="container-fluid" ng-controller="QuoteController">
    <div class="row">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Add Quotes</h4>
                </div>
            </div>
            <div class="card-body ">
                <form id="add_product_form" ng-submit="addQuotes()" accept-charset="multipart/form-data" method="post">
                    <div class="row">
                        {{-- <div class="col-sm-6 input-field" ng-init="getproductList()">
                            <label>Product Name</label>
                            <div class="form-group">
                
                                <select class="select" id="product_option" name="product_option" multiple="multiple" id="product_option" >
                                    
                                    <option ng-repeat="product in products" value="<@ product.id @>"><@ product.product_name @></option>
                            
                                </select>
                                
                            </div>
                        </div> --}}
                        <div class="col-sm-6 input-field">
                            <label>Customer</label>
                            <div class="form-group" ng-init="getcustomertList()">
                                 <select  id="cust_id" name="cust_id" data-style="select-with-transition" title="Single Select" ng-model="cust_id" >
                                    <option value="">--Select user-- </option>
                                    <option ng-repeat="user in users" value="<@ user.id @>"><@ user.name @></option>
                                   
                                </select> 
                             
                                {{-- <select class="form-select form-select-lg mb-3 selectpicker" aria-label=".form-select-lg example">
 
                                    <option value="1">Admin</option>
                                    <option value="2">User</option>

                                </select> --}}
                               
                            </div>
                            
                        </div>
                        {{-- <div class="col-sm-6 input-field">
                            <label>Sales person</label>
                            <div class="form-group" ng-init="getsalestList()">
                                <select  id="product_option" name="product_option" data-style="select-with-transition" title="Single Select" >
                                    <option value="">--Select user-- </option>
                                    <option ng-repeat="user in salesusers" value="<@ user.id @>"><@ user.fname @></option>
                                   
                                </select>
                            </div>
                        </div> --}}
                
                        <div class="col-sm-6 input-field">
                            <label>Company</label>
                            <div class="form-group" ng-init="getcompanyList()">

                                <select class="form-control" ng-change="companySelected()" ng-model="selectedCompany">
                                    <option value="">--Select Company-- </option>
                                    <option ng-repeat="company in companies" value="<@ company.id @>"><@ company.name @></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6 input-field">
                            <label>Sales Person</label>
                            <div class="form-group">
                                <input id="vendor_price" class="form-control" name="vendor_price"  value="<@ companydetails.sales_person_name @>">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Company Website</label>
                            <div class="form-group">
                                <input id="vendor_price" class="form-control" name="vendor_price" value="<@ companydetails.website @>" >
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Company Email </label>
                            <div class="form-group">
                                <input type="text" id="product_price" class="form-control" name="product_price"  value="<@ companydetails.email @>">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Head Office Number</label>
                            <div class="form-group">
                                <input type="number" id="product_weight" class="form-control" name="product_weight" value="<@ companydetails.head_office_number @>">
                            </div>
                        </div>
                        <div class="col-sm-12 input-field">
                            <div class="col-sm-12 input-field">
                                <label>Address</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="address" id="address" ng-model="address">
                                </div>
                            </div>
                            
                        </div>
                    
                    </div>
                    <br>
                    <div  class="col-sm-6 input-field">
                        <label>Search Product</label>
                        <input type="text" class="form-control" placeholder="Enter Product Name" ng-model="searchTest" ng-keyup="ProductSuggest($event)"  >
                    
                       
                        {{-- <div class="input-group-append">
                            <input  value ="search" class="btn btn-primary">
                            {{-- <a href="{{route('home.search')}}"> <span class="input-group-text search-btn">Search</span></a> --}}
                        {{-- </div> --}}
                    </div>

                    <ul id='searchResult' ng-if ="products"   >
                        <li ng-click='setProduct($index,$event)' 
                            ng-repeat="result in products" >
    
                            <@ result.product_name @>
                        </li>
                    
                      </ul> 
                    <div class="table-responsive">
                        <table class="table"  ng-init="getquoteList()">
                          <thead>
                            <tr>
                              {{-- <th>Icon</th> --}}
                              <th>Product Name</th>
                              <th>Quantity</th>
                              <th>Product Price </th>
                              <th>Vendor Price </th>
                              <th>Total Price </th>
                             
                            </tr>
                          </thead>
                          <tbody>
                          
                            <tr ng-class="contacts.tr_class" ng-if ="products"  >
                                <td><@ product_name @></td>
                                <td><input type="number" min="0" max="100" ng-model="quantity" /></td>
                                <td><@ product_price @></td>
                                <td><@ vendor_price @></td>
                                <td  ng-model="total">
                                    <@ quantity * product_price @>
                              </td>
                            </tr>
                           <tr ng-if="horscopeList.length==0"><td colspan="15">No Records Found.</td></tr>
                          </tbody>
                        </table>
                     
                      </div>
             
                    <div class="row">
                        <div class="card-footer">
                            
                             <button class="btn btn-rose" type="submit" id="add_new_btn" name="action">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>     
    </div>
</div>
@endsection

@section('jsScript')
 <script>
    function setFormValidation(id) {
      $(id).validate({
        highlight: function(element) {
          $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
          $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
        },
        success: function(element) {
          $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
          $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
        },
        errorPlacement: function(error, element) {
          $(element).closest('.form-group').append(error);
        },
      });
    }

    $(document).ready(function() {
      setFormValidation('#add_slider_form');
      CKEDITOR.replace('description');
    });

    
  </script>
  <script>

    (function($){
    
    var preloader = $('.preloader');
    setTimeout(function(){
    
    preloader.remove();
    
    }, 2000);
    
    })(jQuery);
    </script> 
  
<script src="{{ asset('admin/js/controller/QuoteController.js') }}"></script>
<script src="{{ asset('admin/js/ckeditor/ckeditor/ckeditor.js') }}"></script>
<script>
        
</script>
@endsection