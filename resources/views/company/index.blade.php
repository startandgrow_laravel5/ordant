@extends('admin.layouts.app')

@section('content')
<style type="text/css">
.img-container.icon_img {
    width: 50%;
}

</style>

<div id="preloaders" class="preloader"></div>
<!-- Custom tips design -->
{{-- <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            
            <div class="card-body">
              <div id="accordion" class="company-list-tips" role="tablist">
                <div class="card-collapse">
                  <div class="card-header" role="tab" id="headingOne">
                    <h5 class="mb-0">
                
                      <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
                        <b>HELPFUL TIPS </b>
                        <i class="material-icons">keyboard_arrow_down</i>
                      </a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                    <div class="card-body">
                      <ul>
                        <li>View all your registered, live & archived learners.</li>
                        <li>When a learner is archived, they no longer show in your account when allocating training.</li>
                        <li>All archived learners are converted into a direct sign-up learner, All allocated training data will be deleted for them but will remain in your account for audit purpose.</li>
                        
                      </ul>  
                    </div>
                  </div>
                </div>
                
                
              </div>
            </div>
          </div>
        </div>  
    </div>
</div> --}}

<!-- End Custom tips design -->
<!-- Form design -->
<div class="container-fluid" ng-controller="CompanyController">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">assignment</i>
                    </div>

                    <h4 class="card-title">Company Search</h4>

                    <div class="col-md-2 pull-right">
                        <a class="btn btn btn-tumblr mb-0 mt-lg-auto w-100">Export</a>
                    </div>
                </div>

                <div class="card-body tips-demo">
                    <form method="post">
                        <div class="row">
                     

                            <div class="col-sm-4 input-field">
                                <label>Company Name</label>
                                <div class="form-group bmd-form-group is-filled">
                                    <input id="name" class="form-control" value="Admin" name="name" required="true" />
                                </div>
                                <input type="hidden" id="id" name="id" value="5" autocomplete="off" />
                            </div>
                            <div class="col-sm-4 input-field">
                                <label>Company Email</label>
                                <div class="form-group bmd-form-group is-filled">
                                    <input id="name" class="form-control" value="Tech Dude" name="name" required="true" />
                                </div>
                                <input type="hidden" id="id" name="id" value="5" autocomplete="off" />
                            </div>
                            <div class="col-sm-4 input-field">
                                <label>Company number</label>
                                <div class="form-group bmd-form-group is-filled">
                                    <input id="name" class="form-control" value="Tech Dude" name="name" required="true" />
                                </div>
                                <input type="hidden" id="id" name="id" value="5" autocomplete="off" />
                            </div>
                        </div>

                        <div class="row pull-right">
                            <div class="card-footer pull-right">
                                <button class="btn btn-rose" type="submit" id="edit_new_btn" name="action">Submit</button>
                                <button class="btn btn-github" type="submit" id="edit_new_btn" name="action">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End form Design -->

<!-- Form list design -->
{{-- <div class="container-fluid" ng-controller="CompanyController">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">assignment</i>
                    </div>
                    <h4 class="card-title">List View</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table main-date">
                            <colgroup>
                                <col span="1" style="width: 75%;" />
                                <col span="1" style="width: 15%;" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td class="td-name">
                                        <img
                                            src="https://media.istockphoto.com/vectors/user-profile-icon-vector-avatar-portrait-symbol-flat-shape-person-vector-id1270368615?k=20&m=1270368615&s=170667a&w=0&h=qpvA8Z6L164ZcKfIyOl-E8fKnfmRZ09Tks7WEoiLawA="
                                            width="30"
                                            class="user-img rounded-circle mr-2"
                                        />
                                        <a href="#">Sdi Learner</a>
                                       
                                    </td>

                                    <td class="td-number text-right">
                                        <button class="btn bg-gradient-primary mb-0">
                                            View Profile
                                            <div class="ripple-container"></div>
                                        </button>
                                    </td>
                                    <td class="td-number">
                                        <div class="togglebutton">
                                            <label>
                                                <input type="checkbox" checked="" />
                                                <span class="toggle"></span>Active
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td-name">
                                        <img
                                            src="https://media.istockphoto.com/vectors/user-profile-icon-vector-avatar-portrait-symbol-flat-shape-person-vector-id1270368615?k=20&m=1270368615&s=170667a&w=0&h=qpvA8Z6L164ZcKfIyOl-E8fKnfmRZ09Tks7WEoiLawA="
                                            width="30"
                                            class="user-img rounded-circle mr-2"
                                        />
                                        <a href="#">Learner</a>
                                        <br />
                                    </td>

                                    <td class="td-number text-right">
                                        <button class="btn bg-gradient-primary mb-0">
                                            View Profile
                                            <div class="ripple-container"></div>
                                        </button>
                                    </td>
                                    <td class="td-number">
                                        <div class="togglebutton">
                                            <label>
                                                <input type="checkbox" checked="" />
                                                <span class="toggle"></span>Active
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td-name">
                                        <img
                                            src="https://media.istockphoto.com/vectors/user-profile-icon-vector-avatar-portrait-symbol-flat-shape-person-vector-id1270368615?k=20&m=1270368615&s=170667a&w=0&h=qpvA8Z6L164ZcKfIyOl-E8fKnfmRZ09Tks7WEoiLawA="
                                            width="30"
                                            class="user-img rounded-circle mr-2"
                                        />
                                        <a href="#">Learner User</a>
                                        <br />
                                    </td>

                                    <td class="td-number text-right">
                                        <button class="btn bg-gradient-primary mb-0">
                                            View Profile
                                            <div class="ripple-container"></div>
                                        </button>
                                    </td>
                                    <td class="td-number">
                                        <div class="togglebutton">
                                            <label>
                                                <input type="checkbox" checked="" />
                                                <span class="toggle"></span>Active
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td-name">
                                        <img
                                            src="https://media.istockphoto.com/vectors/user-profile-icon-vector-avatar-portrait-symbol-flat-shape-person-vector-id1270368615?k=20&m=1270368615&s=170667a&w=0&h=qpvA8Z6L164ZcKfIyOl-E8fKnfmRZ09Tks7WEoiLawA="
                                            width="30"
                                            class="user-img rounded-circle mr-2"
                                        />
                                        <a href="#">Learner Admin</a>
                                        <br />
                                    </td>

                                    <td class="td-number text-right">
                                        <button class="btn bg-gradient-primary mb-0">
                                            View Profile
                                            <div class="ripple-container"></div>
                                        </button>
                                    </td>
                                    <td class="td-number">
                                        <div class="togglebutton">
                                            <label>
                                                <input type="checkbox" checked="" />
                                                <span class="toggle"></span>Active
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<!-- End Form list design -->
<!-- Not user data design -->
{{-- <div class="container-fluid" ng-controller="CompanyController">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  
                  <h4 class="card-title">No Data</h4>
                 
                  
                  
                </div>
                
                <div class="card-body">
                
                  <div class="table-responsive" ng-show="!show_loading">
                  
                    <div ng-if="horscopeList.length==0">
                        <p class="norecord  mR10">
                            <img src="{{ asset('admin/img/nodatafound.svg')}}">
                            <span>No Records Found.</span>
                        </p>
                    </div>
                    <div class="pagination" ng-show="horscopeList">
                    <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageContactChanged()" items-per-page="numPerPage" boundary-links="true" rotate="false" class="pagination-sm" max-size="maxSize"></pagination>
                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
</div> --}}
<!-- End Not user data design -->


<div class="container-fluid" ng-controller="CompanyController">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  
                  <h4 class="card-title">Company List</h4>
                  <div style="float: right;">
                  <a class="btn btn-rose" href="{{ route('company_pdf') }}">Export Orders PDF</a>
                  <a class="btn btn-rose" href="{{ route('company_csv') }}">Export Company CSV</a>
                  </div>
                  <div class="col-md-2 pull-right">
                  <a class="btn bg-gradient-primary mb-0 mt-lg-auto w-100" href="{{ route('companies.create') }}">Add Company</a>
                  </div>
                  <div class="col-md-2 pull-right">
                    <div class="input-group">
                     
                      <input type="text" class="form-control" placeholder="Search Here" ng-model='search'>
                    </div>
                 
                  </div>
                </div>
                
                <div class="card-body">
                <div class="timeline-wrapper"  ng-if="show_loading">
                        <div class="timeline-item course-line-load">
                            <div class="animated-background act-height">
                                <div class="background-masker header-top"></div>
                                <div class="background-masker header-bottom"></div>
                                <div class="background-masker subheader-left"></div>
                                <div class="background-masker subheader-right"></div>
                                <div class="background-masker subheader-bottom"></div>
                                <div class="background-masker content-top"></div>
                                <div class="background-masker content-first-end"></div>
                                <div class="background-masker content-second-line"></div>
                                <div class="background-masker content-second-end"></div>
                                <div class="background-masker content-third-line"></div>
                                <div class="background-masker content-third-end"></div>
                            </div>
                        </div>
                    </div>
                  <div class="table-responsive" ng-show="!show_loading">
                    <table class="table responsive-table mainTable"  ng-init="getcompanyList()">
                      <thead>
                        <tr>
                          {{-- <th>Icon</th> --}}
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone </th>
                          <th class="text-right">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        <tr ng-repeat="horscope in horscopeList | filter:search" ng-class="contacts.tr_class">
                            {{-- <td><div class="img-container icon_img">
                              <img height="100px" weight="100px" src="{{url('/uploads/icon/<@ horscope.icon @>')}}" alt="...">
                            </div>
                            </td> --}}
                            <td><@ horscope.name @></td>
                            <td><@ horscope.email @></td>
                            <td><@ horscope.head_office_number @></td>
                            <td class="td-actions text-right">
                            <a href="{{url('companies/<@ horscope.id @>/edit')}}" type="button" rel="tooltip" class="btn btn-success">
                              <i class="material-icons">edit</i>
                            </a>
                           
                          </td>
                        </tr>
                       <!-- <tr ng-if="horscopeList.length==0"><td colspan="15">No Records Found.</td></tr> -->
                      </tbody>
                    </table>
                    <div ng-if="horscopeList.length==0">
                        <p class="norecord norecord-border mR10">
                            <img src="{{ asset('admin/img/nodatafound.svg')}}">
                            <span>No Records Found.</span>
                        </p>
                    </div>
                    <div class="pagination" ng-show="horscopeList">
                    <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageContactChanged()" items-per-page="numPerPage" boundary-links="true" rotate="false" class="pagination-sm" max-size="maxSize"></pagination>
                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
</div>
@endsection

@section('jsScript')
<script>

      (function($){

      var preloader = $('.preloader');
      setTimeout(function(){

      preloader.remove();

      }, 2000);

      })(jQuery);
 </script> 
 
  <script src="{{ asset('admin/js/controller/CompanyController.js') }}"></script>
  
@endsection