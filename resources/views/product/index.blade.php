@extends('admin.layouts.app') @section('content')
<style type="text/css">
    .img-container.icon_img {
        width: 50%;
    }

</style>
<div id="preloaders" class="preloader"></div>

<div class="container-fluid" ng-controller="ProductController">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Product List</h4>
                  <div class="col-md-2 pull-right">
                  <a class="btn bg-gradient-primary mb-0 mt-lg-auto w-100" href="{{ route('product.create') }}">Add Product</a>
                  </div>
                  <div class="col-md-2 pull-right">
                    <div class="input-group">
                     
                      <input type="text" class="form-control" placeholder="Search Here" ng-model='search'>
                    </div>
                 
                  </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table responsive-table mainTable"  ng-init="getproductList()">
                      <thead>
                        <tr>
                          {{-- <th>Icon</th> --}}
                          <th>Product Name</th>
                          <th>Quantity</th>
                          <th>Product Price </th>
                          <th class="text-right">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        <tr ng-repeat="row in TemplateList | filter:search" ng-class="contacts.tr_class">
                            {{-- <td><div class="img-container icon_img">
                              <img height="100px" weight="100px" src="{{url('/uploads/icon/<@ row.icon @>')}}" alt="...">
                            </div>
                            </td> --}}
                            <td><@ row.product_name @></td>
                            <td><@ row.quantity @></td>
                            <td><@ row.product_price @></td>
                            <td class="td-actions text-right">
                            <a href="{{url('product/<@ row.id @>/edit')}}" type="button" rel="tooltip" class="btn btn-success">
                              <i class="material-icons">edit</i>
                            </a>
                           
                          </td>
                        </tr>
                       <tr ng-if="horscopeList.length==0"><td colspan="15">No Records Found.</td></tr>
                      </tbody>
                    </table>
                    <div class="pagination" ng-show="horscopeList">
                    <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageContactChanged()" items-per-page="numPerPage" boundary-links="true" rotate="false" class="pagination-sm" max-size="maxSize"></pagination>
                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('jsScript')
<script>

(function($){

var preloader = $('.preloader');
setTimeout(function(){

preloader.remove();

}, 2000);

})(jQuery);
</script> 

<script src="{{ asset('admin/js/controller/ProductController.js') }}"></script>
  
@endsection