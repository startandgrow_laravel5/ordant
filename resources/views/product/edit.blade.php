@extends('admin.layouts.app')

@section('content')
<div class="container-fluid" ng-controller="ProductController">
    <div class="row">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Edit {{$productDetails->product_name}}</h4>
                </div>
            </div>
            <div class="card-body ">

                <form id="edit_product_form" ng-submit="editProduct()" accept-charset="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-sm-6 input-field">
                            <label>Product Name</label>
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$productDetails->id}}">
                                <input id="product_name" class="form-control" value="{{$productDetails->product_name}}" name="product_name" required="true">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Product Option</label>
                            <div class="form-group">
                                <select class="selectpicker" id="product_option" name="product_option" data-style="select-with-transition" title="Single Select" >
                                    <option value="Option1">Option1</option>
                                    <option value="option2"> option2</option>
                                  </select>
                            </div>
                            
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>product size</label>
                            <div class="form-group">
                                <input id="product_size" class="form-control" value="{{$productDetails->product_size}}" name="product_size" required="true">
                            </div>
                        </div>
                
                        <div class="col-sm-6 input-field">
                            <label>Quantity</label>
                            <div class="form-group">
                                <input id="quantity" value="{{$productDetails->quantity}}" class="form-control" name="quantity" >
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Vendor Price</label>
                            <div class="form-group">
                                <input id="vendor_price" value="{{$productDetails->vendor_price}}" class="form-control" name="vendor_price" >
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Product price </label>
                            <div class="form-group">
                                <input type="text" id="product_price" value="{{$productDetails->product_price}}" class="form-control" name="product_price" required="true">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Product weight</label>
                            <div class="form-group">
                                <input type="number" id="product_weight" value="{{$productDetails->product_weight}}"  class="form-control" name="product_weight" required="true">
                            </div>
                        </div>
                        
                        <div class="col-sm-6 input-field">
                            <label>product days</label>
                            <div class="form-group">
                                <input type="number" id="product_days" value="{{$productDetails->product_days}}" class="form-control" name="product_days" required="true">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Product sku</label>
                            <div class="form-group">
                                <input type="text" id="product_sku" value="{{$productDetails->product_sku}}" class="form-control" name="product_sku" required="true">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Artwork name</label>
                            <div class="form-group">
                                <input type="text" id="product_days" value="{{$productDetails->product_sku}}"  class="form-control" name="product_days" required="true">
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6">
                            <h4 class="title">Product Image</h4>
                            <div class="fileinput text-center fileinput-exists" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                    <img src="{{ asset('admin/img/image_placeholder.jpg') }}" alt="Add Slider">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"><img src="{{url($productDetails->product_image)}}" alt="{{$productDetails->product_image}}"></div>
                                <div>
                                    <span class="btn btn-rose btn-round btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" id="product_image" name="product_image" >
                                    </span>
                                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                </div>
                            </div>
                        </div>
                        
                    
                      
                       <div class="col-sm-6">
                            <label>Status </label>
                            <div class="form-group select-wizard">
                              <select class="selectpicker" id="status" name="status" data-style="select-with-transition" title="Single Select" >
                                <option value="1" @if($productDetails->status == '1') selected @endif >Active </option>
                                <option value="0" @if($productDetails->status == '2') selected @endif> In Active</option>
                              </select>
                            </div>
                        </div>
                    </div>
               
             
                    <div class="row">
                        <div class="card-footer">
                            
                             <button class="btn btn-rose" type="submit" id="edit_new_btn" name="action">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>     
    </div>
</div>
@endsection

@section('jsScript')
 <script>
    // function setFormValidation(id) {
    //   $(id).validate({
    //     highlight: function(element) {
    //       $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
    //       $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
    //     },
    //     success: function(element) {
    //       $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
    //       $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
    //     },
    //     errorPlacement: function(error, element) {
    //       $(element).closest('.form-group').append(error);
    //     },
    //   });
    // }

    // $(document).ready(function() {
    //   setFormValidation('#add_slider_form');
    //   CKEDITOR.replace('description');
    // });
  </script>
  <script src="{{ asset('admin/js/controller/ProductController.js') }}"></script>
 <script src="{{ asset('admin/js/ckeditor/ckeditor/ckeditor.js') }}"></script>
  <script>
        
    </script>
@endsection