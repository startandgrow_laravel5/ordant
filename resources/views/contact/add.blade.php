@extends('admin.layouts.app')

@section('content')
<div class="container-fluid" ng-controller="ContactController">
    <div class="row">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Add Contact</h4>
                </div>
            </div>
            <div class="card-body ">
                <form id="add_contact_form" ng-submit="addContact()" accept-charset="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-sm-6 input-field">
                            <label>Name</label>
                            <div class="form-group">
                                <input id="name" class="form-control" name="name" required="true">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Country</label>
                            <div class="form-group" ng-init="getcountryList()">
                                <select class="form-control" id="country" name="country">
                                    <option value="">--Select country-- </option>
                                    <option ng-repeat="country in countries" value="<@ country.id @>"><@ country.name @></option>
                                </select>
                            </div>
                            
                        </div>
                        <div class="col-sm-6 input-field" ng-init="getcompanyList()">
                            <label>Company </label>
                            <div class="form-group">
                                <select class="form-control" id="company_id" name="company_id">
                                    <option value="">--Select Company-- </option>
                                    <option ng-repeat="company in companies" value="<@ company.id @>"><@ company.name @></option>
                                </select>
                            </div>
                            
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Email</label>
                            <div class="form-group">
                                <input id="email" class="form-control" name="email" required="true">
                            </div>
                        </div>
                
                        <div class="col-sm-6 input-field">
                            <label>Website</label>
                            <div class="form-group">
                                <input id="website" class="form-control" name="website" >
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Linkedin Profile</label>
                            <div class="form-group">
                                <input id="linkedin_profile" class="form-control" name="linkedin_profile" >
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Facebook URL</label>
                            <div class="form-group">
                                <input type="text" id="facebook_url" class="form-control" name="facebook_url" required="true">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Office Number</label>
                            <div class="form-group">
                                <input type="number" id="head_office_number" class="form-control" name="head_office_number" required="true">
                            </div>
                        </div>
                        
                        <div class="col-sm-6 input-field">
                            <label>Contact Number</label>
                            <div class="form-group">
                                <input type="number" id="secondary_contact_number" class="form-control" name="secondary_contact_number" required="true">
                            </div>
                        </div>
                        <div class="col-sm-6 input-field">
                            <label>Sales Person</label>
                            <div class="form-group">
                                <select class="selectpicker" id="sales_person" name="sales_person" data-style="select-with-transition" title="Single Select" >
                                    <option value="1">User1 </option>
                                    <option value="2">user2</option>
                                  </select>
                                <input type="hidden" id="created_by" class="form-control" name="created_by" value="{{Auth::id()}}" required="true">
                            </div>
                        </div>
                      
                      
                       <div class="col-sm-6">
                            <label>Status </label>
                            <div class="form-group select-wizard">
                              <select class="selectpicker" id="status" name="status" data-style="select-with-transition" title="Single Select" >
                                <option value="1">Active </option>
                                <option value="0"> In Active</option>
                              </select>
                            </div>
                        </div>
                    </div>
               
                    <div class="row">
                        <div class="col-sm-12 input-field">
                            <label>Notes</label>
                            <div class="form-group">
                                <input type="text"class="form-control" name="notes" id="notes">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="card-footer">
                            
                             <button class="btn btn-rose" type="submit" id="add_new_btn" name="action">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>     
    </div>
</div>
@endsection

@section('jsScript')
 <script>
    function setFormValidation(id) {
      $(id).validate({
        highlight: function(element) {
          $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
          $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
        },
        success: function(element) {
          $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
          $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
        },
        errorPlacement: function(error, element) {
          $(element).closest('.form-group').append(error);
        },
      });
    }

    $(document).ready(function() {
      setFormValidation('#add_slider_form');
      CKEDITOR.replace('description');
    });
  </script>
  <script src="{{ asset('admin/js/controller/ContactController.js') }}"></script>
 <script src="{{ asset('admin/js/ckeditor/ckeditor/ckeditor.js') }}"></script>
  <script>
        
    </script>
@endsection