@extends('admin.layouts.app')

@section('content')
<style type="text/css">
.img-container.icon_img {
    width: 50%;
}
</style>
<div id="preloaders" class="preloader"></div>

<div class="container-fluid" ng-controller="ContactController">
          <div class="row">
            <div class="col-md-12">
              
              <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Contact List</h4>
                  <div style="float: right;">
                  <a class="btn btn-rose" href="{{ route('contact_pdf') }}">Export Contact PDF</a>
                  <a class="btn btn-rose" href="{{ route('contact_csv') }}">Export Contact CSV</a>
                  </div>
                  <div class="col-md-2 pull-right">
                  <a class="btn bg-gradient-primary mb-0 mt-lg-auto w-100" href="{{ route('contact.create') }}">Add Contact</a>
                  </div>
                  <div class="col-md-2 pull-right">
                    <div class="input-group">
                     
                      <input type="text" class="form-control" placeholder="Search Here" ng-model='search'>
                    </div>
                 
                  </div>
                </div>
                <div class="card-body">
                    <div class="timeline-wrapper"  ng-if="show_loading">
                        <div class="timeline-item course-line-load">
                            <div class="animated-background act-height">
                                <div class="background-masker header-top"></div>
                                <div class="background-masker header-bottom"></div>
                                <div class="background-masker subheader-left"></div>
                                <div class="background-masker subheader-right"></div>
                                <div class="background-masker subheader-bottom"></div>
                                <div class="background-masker content-top"></div>
                                <div class="background-masker content-first-end"></div>
                                <div class="background-masker content-second-line"></div>
                                <div class="background-masker content-second-end"></div>
                                <div class="background-masker content-third-line"></div>
                                <div class="background-masker content-third-end"></div>
                            </div>
                        </div>
                    </div>
                  <div class="table-responsive" ng-show="!show_loading">
                    <table class="table responsive-table mainTable"  ng-init="getcontactList()">
                      <thead>
                        <tr>
                          {{-- <th>Icon</th> --}}
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone </th>
                          <th class="text-right">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="horscope in contactList | filter:search" ng-class="contacts.tr_class">
                            {{-- <td><div class="img-container icon_img">
                              <img height="100px" weight="100px" src="{{url('/uploads/icon/<@ horscope.icon @>')}}" alt="...">
                            </div>
                            </td> --}}
                            <td><@ horscope.name @></td>
                            <td><@ horscope.email @></td>
                            <td><@ horscope.head_office_number @></td>
                            <td class="td-actions text-right">
                            <a href="{{url('contact/<@ horscope.id @>/edit')}}" type="button" rel="tooltip" class="btn btn-success">
                              <i class="material-icons">edit</i>
                            </a>
                           
                          </td>
                        </tr>
                       <!-- <tr ng-if="contactList.length==0"><td colspan="15">No Records Found.</td></tr> -->
                      </tbody>
                    </table>
                    <div ng-if="contactList.length==0">
                        <p class="norecord norecord-border mR10">
                            <img src="{{ asset('admin/img/nodatafound.svg')}}">
                            <span>No Records Found.</span>
                        </p>
                    </div>
                    <div class="pagination" ng-show="contactList">
                    <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageContactChanged()" items-per-page="numPerPage" boundary-links="true" rotate="false" class="pagination-sm" max-size="maxSize"></pagination>
                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('jsScript')

<script>

  (function($){

  var preloader = $('.preloader');
  setTimeout(function(){

  preloader.remove();

  }, 2000);

  })(jQuery);
</script> 

 
  <script src="{{ asset('admin/js/controller/ContactController.js') }}"></script>
  
@endsection