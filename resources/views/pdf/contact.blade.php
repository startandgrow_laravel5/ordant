<html>

<head>
    <title>PDF</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style type="text/css"></style>
</head>
<body>
    <div class="row">
        <div class="col-md-12">
            <table id="tabelapadrao" class="table table-condensed table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Company Name</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone No.</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($contact as $data)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $data->company_name }}</td>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->head_office_number }}</td>
                        <td>
                            @if($data->status==0) Active @endif
                            @if($data->status==1) Inactive @endif
                        </td>
                    </tr>
                    @php $i++; @endphp
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</body>
</html>