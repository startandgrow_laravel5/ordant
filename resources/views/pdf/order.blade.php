<html>

<head>
    <title>PDF</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style type="text/css"></style>
</head>
<body>
    <div class="row">
        <div class="col-md-12">
            <table id="tabelapadrao" class="table table-condensed table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Order No</th>
                        <th>Supplier Name</th>
                        <th>Customer name</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($order as $data)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $data->order_no }}</td>
                        <td>{{ $data->supplier_name }}</td>
                        <td>{{ $data->customer_name }}</td>
                        <td>{{ $data->total }}</td>
                    </tr>
                    @php $i++; @endphp
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</body>
</html>