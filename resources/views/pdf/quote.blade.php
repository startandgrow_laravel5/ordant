<html>

<head>
    <title>PDF</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style type="text/css"></style>
</head>
<body>
    <div class="row">
        <div class="col-md-12">
            <table id="tabelapadrao" class="table table-condensed table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Order No</th>
                        <th>Supplier Name</th>
                        <th>Customer name</th>
                        <th>Total</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($quote as $data)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $data->order_no }}</td>
                        <td>{{ $data->supplier_name }}</td>
                        <td>{{ $data->customer_name }}</td>
                        <td>{{ $data->total }}</td>
                        <td>
                            @if($data->status==0) Pending @endif
                            @if($data->status==1) Accepted @endif
                            @if($data->status==2) Revision @endif
                            @if($data->status==3) Canceled @endif
                        </td>
                    </tr>
                    @php $i++; @endphp
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</body>
</html>