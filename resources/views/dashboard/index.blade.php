@extends('admin.layouts.app') @section('content')
<div id="preloaders" class="preloader"></div>
<div class="content" ng-controller="DashboardController">
    <div class="container-fluid" ng-init="getDashboardList()">
        <div class="row">
        <div class="col">
                <div class="card mb-3 widget-content bg-midnight-bloom">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Company</div>
                            {{-- <div class="widget-subheading">Loren Ipsum</div>   --}}
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span><@ dasboardcount.company_count @></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card mb-3 widget-content card-header-rose">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Orders</div>
                            {{-- <div class="widget-subheading">Loren Ipsum</div>   --}}
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span><@ dasboardcount.order_count @></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card mb-3 widget-content bg-grow-early">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Contacts</div>
                            {{-- <div class="widget-subheading">Loren Ipsum</div>  --}}
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span><@ dasboardcount.contact_count @></span></div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="col">
                <div class="card mb-3 widget-content card-header-warning">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Product</div>
                            {{-- <div class="widget-subheading">Loren Ipsum</div>   --}}
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span><@ dasboardcount.products_count @></span></div>
                        </div>
                    </div>
                </div>
            </div>
          
    
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-success card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons"></i>
                        </div>
                        <h4 class="card-title">Top 5 Company List</h4>
                    </div>
                    <div class="card-body" >
                        <div class="table-responsive">
                            <table class="table responsive-table mainTable"  >
                                <thead>
                                  <tr>
                                    {{-- <th>Icon</th> --}}
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone </th>
                                    <th class="text-right">Actions</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  
                                  <tr ng-repeat="horscope in dasboardcompanies " ng-class="contacts.tr_class">
                                      {{-- <td><div class="img-container icon_img">
                                        <img height="100px" weight="100px" src="{{url('/uploads/icon/<@ horscope.icon @>')}}" alt="...">
                                      </div>
                                      </td> --}}
                                      <td><@ horscope.name @></td>
                                      <td><@ horscope.email @></td>
                                      <td><@ horscope.head_office_number @></td>
                                      <td class="td-actions text-right">
                                      <a href="{{url('companies/<@ horscope.id @>/edit')}}" type="button" rel="tooltip" class="btn btn-success">
                                        <i class="material-icons">edit</i>
                                      </a>
                                     
                                    </td>
                                  </tr>
                                 <!-- <tr ng-if="horscopeList.length==0"><td colspan="15">No Records Found.</td></tr> -->
                                </tbody>
                              </table>
                           
                        </div>
                    </div>
                </div>
            </div> 
        </div>       
        
      
     
    </div>
</div>
@endsection @section('jsScript')
<script>

    (function($){

    var preloader = $('.preloader');
    setTimeout(function(){

    preloader.remove();

    }, 2000);

    })(jQuery);
</script> 

<script src="{{ asset('admin/js/controller/DashboardController.js') }}"></script>

@endsection
