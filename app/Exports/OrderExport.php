<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Orders;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrderExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Orders::select('order')
        ->leftJoin('companies', 'companies.id', '=', 'order.supl_id')
        ->leftJoin('contacts', 'contacts.id', '=', 'order.cust_id')
        ->leftJoin('estimates', 'estimates.id', '=', 'order.est_id')
        ->select('order.id','estimates.project_name','contacts.name as customername','companies.name as suppliername','order.order_no','order.order_date','order.discount','order.discount_per','order.tax','order.tax_per','order.total')
        ->orderBy('order.id', 'DESC')
        ->get();
    }
    public function headings(): array
    {
        return [
        'ID',
        'Project Name',
        'Customer Name',
        'Supplier Name',
        'Order No.',
        'Order Date',
        'Discount',
        'Discount Percentage',
        'Tax',
        'Tax Percentage',
        'Total'
        ];
    }
}
