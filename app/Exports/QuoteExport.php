<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Quotes;
use Maatwebsite\Excel\Concerns\WithHeadings;

class QuoteExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Quotes::select('quotation')
        ->leftJoin('companies', 'companies.id', '=', 'quotation.supl_id')
        ->leftJoin('contacts', 'contacts.id', '=', 'quotation.cust_id')
        ->leftJoin('estimates', 'estimates.id', '=', 'quotation.est_id')
        ->select('quotation.id','estimates.project_name','contacts.name as customername','companies.name as suppliername','quotation.order_no','quotation.order_date','quotation.discount','quotation.discount_per','quotation.tax','quotation.tax_per','quotation.total','quotation.status')
        ->orderBy('quotation.id', 'DESC')
        ->get();
    }
    public function headings(): array
    {
        return [
            'ID',
            'Project Name',
            'Customer Name',
            'Supplier Name',
            'Order No.',
            'Order Date',
            'Discount',
            'Discount Percentage',
            'Tax',
            'Tax Percentage',
            'Total',
            'Status'
            ];
    }
}
