<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Contact;
use Maatwebsite\Excel\Concerns\WithHeadings;
class ContactExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Contact::select('name', 'email', 'website', 'linkedin_profile', 'facebook_url', 'head_office_number', 'secondary_contact_number')
        ->orderBy('id', 'DESC')
        ->get();
    }
     public function headings(): array
    {
        return [
       'Name',  
       'Email',
       'Website',
       'Linkedin Profile',
       'Facebook Profile',
       'Contact Number',
       'Secondary Contact Number'
        ];
    }
}
