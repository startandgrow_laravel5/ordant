<?php

namespace App\Models\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class CommonModel extends Model
{
    public static function getAllData($table_name, $field_id)
    {
        return DB::table($table_name)->orderBy($field_id, 'ASC')->get();
    }
    public static function insertData($table_name, $data)
    {
        return DB::table($table_name)->insertGetId($data);
    }
    public static function getAllDatas($table_name, $field_id)
    {
        return DB::table($table_name)->orderBy($field_id, 'DESC')->get();
    }
    public static function getDataById($table_name, $field_id, $id)
    {
        return DB::table($table_name)->where($field_id, $id)->get();
    }
    public static function getDataByIdTwoCondition($table_name, $field1, $value1, $field2, $value2)
    {
        $avg = DB::table($table_name)
            ->where($field1, '=', $value1)
            ->where($field2, '=', $value2)->get();

        return $avg;
    }
    public static function updateData($table_name, $field_id, $data, $id)
    {
        return DB::table($table_name)->where($field_id, $id)->update($data);
    }

    public static function deleteData($table_name, $field_id, $data, $id)
    {
        return DB::table($table_name)->where($field_id, $id)->update($data);
        //        return DB::table($table_name)->where($field_id, $id)->delete();
    }
    public static function checkUniqueData($table_name, $field_name, $name)
    {
        $query = DB::table($table_name)->where($field_name, $name)->where('is_delete', 0)->count();
        if ($query > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public static function checkUniqueDataEdit($table_name, $field_name, $name, $field_id, $id)
    {
        $query = DB::table($table_name)->where($field_name, $name)->where($field_id, '!=', $id)->where('is_delete', 0)->count();
        if ($query > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public static function getSearch($key)
    {
        $query =  DB::table('products')
            ->Where(function ($query) use ($key) {
                $query->orwhere('product_name', 'like', "%$key%")
                    ->orwhere('artwork_name', 'like', "%$key%");
            })
            ->get();
        return $query;
    }
    public static function getComment($id)
    {
        return DB::table('quote_comments')
            ->leftJoin('contacts', 'contacts.id', '=', 'quote_comments.customer_id')
            ->select('quote_comments.*', 'contacts.name as customer_name')
            ->where('quote_comments.order_id', $id)
            ->orderBy('quote_comments.id', 'DESC')
            ->get();
    }
}
