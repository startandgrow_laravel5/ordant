<?php

namespace App\Models\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;


class LoginModel extends Model
{

    public static function checkLogin($email)
    {
        $q = DB::table('users')
            ->select('*')
            ->Where('email', '=', $email);
        return $q->get();
    }
    
}
