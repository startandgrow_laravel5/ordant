<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    use HasFactory;
    protected $table = 'site_setting';
    protected $fillable = [
        'identifier',
        'name',
        'type',
        'updated_by',
        'created_by',
    ];
}
