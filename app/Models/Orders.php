<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\OrderDetail;

class Orders extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'order';

    /**
     * Relationship: order_id table orderdetails models
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function OrderDetails()
    {
        return $this->belongsTo(OrderDetail::class, 'order_id');
    }
    /**
     * Basic Rules.
     * @param int $id
     * @return array
     */
}
