<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Company extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    protected $fillable = [
     
        'product_name',
        'product_price',
        'created_by',
   

  ];
    protected $table = 'companies';
    public static function rules($id = 0)
    {
        $rules = [
            "country" => "required",
            "name" => "required",
            "email " => "required",
            "status " => "required",
        ];

        return $rules;
    }

    
    /**
     * Basic Rules.
     * @param int $id
     * @return array
     */
    public static function rulesMessage($id = 0)
    {
        $ruleMessage = [
            "country.required" => "Country is required",
            "name.required" => "Name is required",
            "email.required" => "Email field is required",
            "status.required" => "Status field is required",
        ];
        return $ruleMessage;
    }
    
}
