<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailTemplate extends Authenticatable
{
    use HasFactory;
    protected $keyType = 'string';

    public $timestamps = true;
    
    protected $fillable = [
        'template_type', 'subject', 'content', 'sms_content','status','created_at','updated_at','deleted_at',
    ];

    public static function rules($id = 0)
    {
        $rules = [
            "template_type" => "required",           
            "subject" => "required",
            "content" => "required",
            "sms_content" => "required",
            "status" => "required",
        ];

        return $rules;
    }

  public static function rulesMessage($id = 0)
    {
        $ruleMessage = [
            "template_type.required" => "Template Type is required",
            "subject.required" => "Subject is required",
            "content.required" => "Content is required",
            "sms_content.required" => "SMS Content is required",
            "status.required" => "Status is required",
           
        ];
        return $ruleMessage;
    }
}
