<?php

namespace App\Classes;
use Mail;
use PHPMailer\PHPMailer\PHPMailer;

Class SendMail {
	
	public function sendMail($sendermail, $subject, $msg){
        ini_set("SMTP", "sg2plcpnl0112.prod.sin2.secureserver.net");
        ini_set("smtp_port", "465");

//        require_once(public_path . '/phpmailer/class.phpmailer.php');

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->addreplyto(config('constant.USER_NAME'), "Reply");
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = config('constant.PROTOCOLS');
        $mail->Host = config('constant.HOST');
        $mail->Port = config('constant.PORT');
        $mail->IsHTML(true);
        $mail->Username = config('constant.USER_NAME');
        $mail->Password = config('constant.PASSWORD');
        $mail->setfrom(config('constant.USER_NAME'));
        $mail->Subject = $subject;
        $mail->FromName = 'OMS';
        $body = $msg;
        $mail->Body = $body;
        $mail->AddAddress($sendermail);
        if (!$mail->Send()) {
            return false;
        } else {
//             $this->session->set_flashdata('success', 'Your Detail Sent Successfully');
            return true;
        }
		  $mail->smtpClose();
	}
}