<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Api\LoginModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use App\MSG91;
use Carbon\Carbon;
class UserController extends Controller
{

     /**
     * Function to register user .
     *
     * @return Response
     */
    public function loginUser(Request $request){

        $loginData = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $auth = Auth::guard('web');
        if ($auth instanceof \Illuminate\Contracts\Auth\StatefulGuard){
            if ($auth->attempt($loginData))
            {
            $users=auth()->user();
            
            if ( isset($users['mobile']) && $users['mobile'] =="" ) {
                $response['error'] = 1;
                $response['message'] = 'Invalid mobile number';
                $response['loggedIn'] = 1;
            } else {
        
                $otp = rand(100000, 999999);
               $MSG91 = new MSG91();
        
               $msg91Response = $MSG91->sendSMS($otp,$users['mobile']);
        
                if($msg91Response['error']){
                    $response['error'] = 1;
                    $response['message'] = $msg91Response['message'];
                    $response['loggedIn'] = 1;
                }else{
        
                    Session::put('OTP', $otp);
        
                    $response['error'] = 0;
                    $response['message'] = 'Your OTP is created.';
                    $response['OTP'] = $otp;
                    $response['loggedIn'] = 1;
                    $response['token'] = auth()->user()->createToken('API Token')->plainTextToken;
                }
            }
            return response()->json([
                'error' => [
                    'null'
                ],
                'data' =>
                $response,


            ], 200);

        }
        return response()->json([
            'error' => [
                'messege' => 'Something went wrong |try Again',
                'errorCode' => '404'
            ],
            'data' => []


        ], 404);

        }
    }

    /**
    * Function to loginUser.
    *
    * @return Response
    */
    public function registerUser(Request $request){
        $validatedData = $request->validate([
            'fname' => 'required|max:55',
            'lname' => 'required|max:55',
            'username' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'contact_number' => 'required',
            'password' => 'required|confirmed'
        ]);

        $validatedData['password'] = bcrypt($request->password);
        $user = User::create($validatedData);

        $accessToken = $user->createToken('API Token')->plainTextToken;

        return response([ 'user' => $user, 'access_token' => $accessToken]);
        if (!$user) {

            return response()->json([
                'error' => [
                    'messege' => 'Something went wrong |try Again',
                    'errorCode' => '404'
                ],
                'data' => []


            ], 404);
        }

        return response()->json([
            'error' => [
                'null'
            ],
            'data' => $user,
            'access_token' => $accessToken


        ], 200);

    }

    /**
    * Function to verify OTP.
    *
    * @return Response
    */
    public function verifyOtp(Request $request){

        $response = array();

        $enteredOtp = $request->input('otp');
        $userId = Auth::user()->id;  //Getting UserID.

        if($userId == "" || $userId == null){
            $response['error'] = 1;
            $response['message'] = 'You are logged out, Login again.';
            $response['loggedIn'] = 0;
        }else{
            $OTP = $request->session()->get('OTP');
            print_r($OTP);exit;
            if($OTP === $enteredOtp){

                // Updating user's status "isVerified" as 1.

                User::where('id', $userId)->update(['active' => 1]);

                //Removing Session variable
                Session::forget('OTP');

                $response['error'] = 0;
                $response['isVerified'] = 1;
                $response['loggedIn'] = 1;
                $response['message'] = "Your Number is Verified.";
            }else{
                $response['error'] = 1;
                $response['isVerified'] = 0;
                $response['loggedIn'] = 1;
                $response['message'] = "OTP does not match.";
            }
        }
        echo json_encode($response);
    }
    
    /**
     * Function to register user .
     *
     * @return Response
     */
    // public function login(Request $request)
    // {
    //     $email = $request->email;
    //     $pass = $request->password;
    //     if ($email != '') {
    //         if ($pass != '') {
    //             @$user = LoginModel::checkLogin($request->email)->first();
    //             @$verify = password_verify(@$request->password, @$user->password);
    //             if (@$user->disable_account == 0) {
    //                 if (@$verify) {
    //                     $datas['user_id'] = $user->id;
    //                     $datas['fname'] = $user->fname;
    //                     $datas['lname'] = $user->lname;
    //                     $datas['username'] = $user->username;
    //                     $datas['email'] = $user->email;
    //                     $datas['sales_person_id'] =  $user->sales_person_id;
    //                     $datas['contact_number'] = $user->contact_number;
    //                     $datas['otis_permissionp'] = $user->is_permission;
    //                     $datas['is_default_sales_person'] = $user->is_default_sales_person;
    //                     $datas['country_id'] = $user->country_id;
    //                     $datas['region_id'] = $user->region_id;
    //                     $datas['town_id'] = $user->town_id;
    //                     $datas['company_account'] = $user->company_account;
    //                     $datas['postcode'] = $user->postcode; 
    //                     {
    //                         session(['user' => $datas]);
    //                         return response()->json(['status' => true, 'message' => "Logged In Successfull", 'data' => $datas]);
    //                     }
    //                 } else {
    //                     return response()->json(['status' => false, 'message' => "Username and/or Password is not correct"]);
    //                 }
    //             } else {
    //                 return response()->json(['status' => false, 'message' => "Your Account is Disabled Please Contact Administrator."]);
    //             }
    //         } else {
    //             return response()->json(['status' => false, 'message' => "Password Required"]);
    //         }
    //     } else {

    //         return response()->json(['status' => false, 'message' => "Email Required"]);
    //     }
    // }
    public function sales_person_list()
    {
        $company = User::where('sales_person_id','1')->orderBy('id', 'DESC')->get();
       // print_r($company);
        if ($company) {
            $companies = $company;
        } else {
            $companies = [];
        }
        return response()->json(['status' => true,'data' => $companies]);
        // return Response::json([
        //     'error' => '',
        //     'data' => $companies,
        // ], 200);
    }
    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($data)) {
            return response(['message' => 'Invalid Credentials']);
          }
         
          $accessToken = auth()->user()->createToken('authToken')->accessToken;
          return response(['user' => auth()->user(), 'access_token' => $accessToken]);

    }
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'mobile' => 'required',
            'password' => 'required|confirmed'
        ]);
        $validatedData['password'] = bcrypt($request->password);
        $validatedData['active'] = 1;

        $user = User::create($validatedData);

        $accessToken = $user->createToken('API_Token')->plainTextToken;

        return response(['user' => $user, 'access_token' => $accessToken]);
    }
    public function userdetails() 
    {
        $userdetail=auth()->user();
        if (!$userdetail) {

            return Response::json([
                'error' => [
                    'messege' => 'Something went wrong |try Again',
                    'errorCode' => '404'
                ],
                'data' => []
    
    
            ], 404);
        }
    
        return Response::json([
            'error' => [
                'null'
            ],
            'data' => $userdetail,
        ], 200);
    }
    public function logout()
    {
    
        
        Session::flush();

        return Response::json([
            'error' => [
                'null'
            ],
            'message' => 'Logout Successfull'
        ], 200);
    }


    /**
     * Function to verify OTP.
     *
     * @return Response
     */
   



}
