<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;
use Validator;
use App\Models\Company;
use App\Models\Api\CommonModel;

class CompanyController extends Controller

{
    /**
     * Function to register user .
     *
     * @return Response
     */
    public function company_list()
    {
        // $company = Company::orderBy('id','DESC')->paginate(15);
        $company = Company::select('companies')
            ->leftJoin('users', 'users.id', '=', 'companies.sales_person')
            ->select('companies.*', 'users.username as sales_person_name')
            ->orderBy('companies.id', 'DESC')
            ->paginate(15);
        if ($company) {
            $companies = $company;
        } else {
            $companies = [];
        }
        return response()->json(['status' => true, 'data' => $companies]);
    }

    public function company_update(Request $request)
    {

        $id = $request->id;
        if ($id != "") {
            $data = array(
                'country' => $request->country,
                'name' => $request->name,
                'email' => $request->email,
                'website' => $request->website,
                'linkedin_profile' => $request->linkedin_profile,
                'facebook_url' => $request->facebook_url,
                'head_office_number' =>  $request->head_office_number,
                'secondary_contact_number' =>  $request->secondary_contact_number,
                'sales_person' =>  $request->sales_person,
                'status' =>  $request->status,
                'notes' =>  $request->notes,
                'updated_at' =>  date('Y-m-d H:i:s'),
            );
            $res = CommonModel::updateData('companies', 'id', $data, $id);
            return response()->json(['status' => true, 'message' => "Company Update Successfully"]);
        } else {
            return response()->json(['status' => false, 'message' => "Company Id Required."]);
        }
    }

    public function insert_company(Request $request)
    {
        $data = array(
            'country' => $request->country,
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
            'linkedin_profile' => $request->linkedin_profile,
            'facebook_url' => $request->facebook_url,
            'head_office_number' =>  $request->head_office_number,
            'secondary_contact_number' =>  $request->secondary_contact_number,
            'sales_person' =>  $request->sales_person,
            'status' =>  $request->status,
            'notes' =>  $request->notes,
            'created_at' =>  date('Y-m-d H:i:s'),
        );
        $res = CommonModel::insertData('companies', $data);
        return response()->json(['status' => true, 'message' => "Company Added Succesfully", 'data' => $data]);
    }
    // public function insert_company(Request $request)
    // { 
    //    $validatedData = Validator::make($request->all(),[
    //         'name' => 'required',                   
    //         'email' => 'required|unique:companies',           
    //         'country' => 'required',                  
    //         'notes' => 'required',
    //         'status' => 'required',
    //         'linkedin_profile' => 'required',
    //         'facebook_url' => 'required',
    //         'head_office_number' => 'required',
    //         'secondary_contact_number' => 'required',
    //         'website' => 'required',
    //         'sales_person' => 'required' ,       
    //     ]);

    //     if ($validatedData->fails()) {          

    //        return Response::json([                       
    //                         'messege' => $validatedData->messages()->all(),
    //                         'errorCode' => '404'
    //                     ], 404);
    //     }
    //     if ($validatedData)
    //     {             
    //         if (!Company::create($request->all())) {
    //             return Response::json([
    //                 'error' => [
    //                     'messege' => 'Something went wrong |try Again',
    //                     'errorCode' => '404'
    //                 ],
    //                 'data' => []   
    //             ], 404);
    //         } else {   
    //             return Response::json([
    //                 'error' => [
    //                     'null'
    //                 ],
    //                 'data' => 'Data Inserted successfully',
    //             ], 200);
    //          }
    //     }
    // }


    public function destroy(Request $request, $id)
    {
        if ($id > 0) {
            if (Company::find($id)->delete()) {
                return Response::json([
                    'Message' => '',
                    'data' => 'Data Deleted Successfully',
                ], 200);
            } else {
                return Response::json([
                    'messege' => 'Something went wrong |try Again',
                    'errorCode' => '404'
                ], 404);
            }
        } else {
            return Response::json([
                'messege' => 'No Company found',
                'errorCode' => '404'
            ], 404);
        }
    }
    public function company_detail($id)
    {
        $company = Company::findorFail($id); //searches for the object in the database using its id and returns it.
        if ($company->sales_person) {
            $company = Company::select('companies')
            ->leftJoin('users', 'users.id', '=', 'companies.sales_person')
            ->select('companies.*', 'users.username as sales_person_name')
            ->where('companies.id', $id)
            ->orderBy('companies.id', 'DESC')
            ->first();
        }
        return response()->json([
            'data' => $company,
            'message' => 'Company get successfully',
        ], 200);
    }
    public function country_list()
    {
        $countries = CommonModel::getAllData('countries','id');
        return response()->json([
            'data' => $countries,
            'message' => 'Countries get successfully',
        ], 200);
    }
}
