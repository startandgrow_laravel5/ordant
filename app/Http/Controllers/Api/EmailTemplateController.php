<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;
use Validator;
use App\Models\EmailTemplate;
use DB;
class EmailTemplateController extends Controller
{
    /**
     * Function to Api of spacopmlex add.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $validatedData = Validator::make($request->all(), [
            'name' => 'required',
            'template_type' => 'required',
            'subject' => 'required',
            'content' => 'required',
            'status' => 'required',
        ]);
        if ($validatedData->fails()) {
            return Response::json(['status' => false,'messege' => $validatedData->messages()->first(),'data' => []], 404);
        }
        $input = $request->all();
        //check template exit
        $check = EmailTemplate::where('template_type',$input['template_type'])->count();

        if($check>0){
            $res = array('message'=>'Type already selected! Please select another type.','status'=>true);
            return response()->json($res);
        }
        
        $addTemplate = new EmailTemplate();
        $addTemplate->name = $input['name'];
        $addTemplate->template_type = $input['template_type'];
        $addTemplate->subject = $input['subject'];
        $addTemplate->content = $input['content'];
        $addTemplate->sms_content = $input['sms_content'];
        $addTemplate->status = $input['status'];
        $addTemplate->save();

        if($addTemplate)
            $res = array('message'=>'Template created successfully.','status'=>true);
        else
            $res = array('message'=>'Something went wrong!!!, Please try again.','status'=>false);
            
        return response()->json($res);
    }
    
    
    /**
    * Function for delete lead status 
    * @param Request $request
    * @param $id as id of lead status
    * @return json response
    */
    public function deleteStatus($id, Request $request){
        //Update Trash Value To 1
        $updateTrash = \App\LeadStatus::where('id',$id)->update(array('trash'=>'1'));
        
        if($updateTrash)
            $res[] = array('message'=>'Status deleted successfully.','status'=>true);
        else
            $res[] = array('message'=>'Something went wrong!!!, Please try again.','status'=>false);
        return response()->json($res);
    }

    public function show($id)
    {
        $contact = EmailTemplate::findorFail($id); //searches for the object in the database using its id and returns it.

        return response()->json([
            'data' => $contact,
            'message' => 'Email Template get successfully',
        ], 200);
    }
    public function update(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'name' => 'required',
            'template_type' => 'required',
            'subject' => 'required',
            'content' => 'required',
            'status' => 'required',
        ]);

        if ($validatedData->fails()) {
            return Response::json([
                'status' => false,
                'messege' => $validatedData->messages()->all(),
                'data' => []
            ], 404);
        }
        $datas = $request->all();
        $id = $datas['id'];

        //check template exit
        $check = EmailTemplate::where('template_type',$datas['template_type'])->where('id', '<>' , $id)->count();

        if($check>0){
            $res = array('message'=>'Type already selected! Please select another type.','status'=>true);
            return response()->json($res);
        }
        
        $updateStatus = EmailTemplate::find($id);

        unset($datas['id']);

        $updateStatus->update($datas);

        if($updateStatus)
            $res = array('message'=>'Templete updated successfully.','status'=>true);
        else
            $res = array('message'=>'Something went wrong!!!, Please try again.','status'=>false);
        
        return response()->json($res);
    }
    public function index(Request $request)
    {
        $template = EmailTemplate::orderBy('created_at', 'DESC')->paginate(15);
        if (count($template) > 0) {
            return Response::json([
                'status' => true,
                'message' => 'Templete List.',
                'data' => $template,
            ], 200);
        } else {
            return Response::json([
                'status' => false,
                'messege' => 'No contacts found',
                'errorCode' => '404'
            ], 404);
        }
    }
    public function destroy(Request $request, $id)
    {
        if ($id > 0) {
            if (EmailTemplate::find($id)->delete()) {
                return Response::json([
                    'status' => true,
                    'Message' => 'Data Deleted Successfully'
                ], 200);
            } else {
                return Response::json([
                    'status' => false,
                    'messege' => 'Something went wrong |try Again'
                ], 404);
            }
        } else {
            return Response::json([
                'status' => false,
                'messege' => 'No Contacts found'
            ], 404);
        }
    }
}
