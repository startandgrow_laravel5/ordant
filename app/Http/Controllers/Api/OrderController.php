<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\Quotes;
use App\Models\OrderDetail;
use App\Models\Contact;
use Illuminate\Support\Facades\Response;
use App\Models\Api\CommonModel;
use Illuminate\Http\Request;
use Validator;
use DB;
use Mail;

class OrderController extends Controller
{
    /**
     * Function to register user .
     *
     * @return Response
     */
    public function order_insert(Request $request)
    {
        $latestOrder = Quotes::orderBy('id', 'DESC')->first();
        if (@$latestOrder->order_no) {
            $order_no = $latestOrder->order_no + 1;
        } else {
            $order_no = 100000;
        }
        $data = array(
            'est_id' => 1,
            'cust_id' => $request->cust_id,
            'supl_id' => $request->supl_id,
            'order_no' => $order_no,
            'order_date' => date('Y-m-d'),
            'total' => $request->total,
        );
        $res = CommonModel::insertData('quotation', $data);
        $data = DB::table('order_details')->insertGetId([
            'order_id' => $res,
            'product_id' => $request->product_id,
            'total' => $request->total,
            'qty' => $request->qty,
            'created_at' => now()
        ]);
        return response()->json(['status' => true, 'message' => "Order Added Succesfully", 'data' => $data]);
    }
    public function order_detail($id)
    {
        $data['order'] = Quotes::findorFail($id);
        $data['products'] = OrderDetail::select('order_details')
            ->leftJoin('products', 'products.id', '=', 'order_details.product_id')
            ->select('order_details.id as order_detils_id', 'order_details.discount', 'order_details.discount_per', 'order_details.qty', 'order_details.total', 'products.id as product_id', 'products.product_name', 'products.product_size', 'products.vendor_price', 'products.artwork_name', 'products.product_image')
            ->where('order_details.order_id', $id)
            ->orderBy('order_details.id', 'DESC')
            ->get();
        $data['comments'] = CommonModel::getDataById('quote_comments', 'order_id', $id);
        //OrderDetail::where('order_id', $id)->get();
        return response()->json([
            'data' => $data,
            'message' => 'Order get successfully',
        ], 200);
    }
    public function comment_list($id)
    {
        $comments = CommonModel::getComment($id);
        return response()->json([
            'data' => $comments,
            'message' => 'Comments get successfully',
        ], 200);
    }

    public function order_list($id)
    {
        if ($id == 1) {
            $order = Orders::select('order')
                ->leftJoin('companies', 'companies.id', '=', 'order.supl_id')
                ->leftJoin('contacts', 'contacts.id', '=', 'order.cust_id')
                ->leftJoin('estimates', 'estimates.id', '=', 'order.est_id')
                ->select('order.*', 'companies.name as supplier_name', 'companies.id', 'estimates.project_name', 'contacts.name as customer_name')
                ->orderBy('order.id', 'DESC')
                ->paginate(15);
        } else {
            $order = Orders::select('order')
                ->leftJoin('companies', 'companies.id', '=', 'order.supl_id')
                ->leftJoin('contacts', 'contacts.id', '=', 'order.cust_id')
                ->leftJoin('estimates', 'estimates.id', '=', 'order.est_id')
                ->select('order.*', 'companies.name as supplier_name', 'companies.id', 'estimates.project_name', 'contacts.name as customer_name')
                ->where('order.cust_id', $id)
                ->orderBy('order.id', 'DESC')
                ->paginate(15);
        }
        if ($order) {
            $orders = $order;
        } else {
            $orders = [];
        }
        return Response::json([
            'error' => '',
            'data' => $orders,
        ], 200);
    }
    public function quote_list($id)
    {
        $user = auth()->user();


        if ($id == 1) {
            $order = Quotes::select('quotation')
                ->leftJoin('companies', 'companies.id', '=', 'quotation.supl_id')
                ->leftJoin('contacts', 'contacts.id', '=', 'quotation.cust_id')
                ->leftJoin('estimates', 'estimates.id', '=', 'quotation.est_id')
                ->select('quotation.*', 'companies.name as supplier_name', 'companies.id as company_id', 'estimates.project_name', 'contacts.name as customer_name')
                ->orderBy('quotation.id', 'DESC')
                ->paginate(15);
        } else {
            $order = Quotes::select('quotation')
                ->leftJoin('companies', 'companies.id', '=', 'quotation.supl_id')
                ->leftJoin('contacts', 'contacts.id', '=', 'quotation.cust_id')
                ->leftJoin('estimates', 'estimates.id', '=', 'quotation.est_id')
                ->select('quotation.*', 'companies.name as supplier_name', 'companies.id as company_id', 'estimates.project_name', 'contacts.name as customer_name')
                ->where('quotation.cust_id', $id)
                ->orderBy('quotation.id', 'DESC')
                ->paginate(15);
        }
        if ($order) {
            $orders = $order;
        } else {
            $orders = [];
        }
        return Response::json([
            'error' => '',
            'data' => $orders,
        ], 200);
    }
    public function destroy(Request $request, $id)
    {
        if ($id > 0) {
            if (Orders::find($id)->delete()) {
                return Response::json([
                    'Message' => '',
                    'data' => 'Data Deleted Successfully',
                ], 200);
            } else {
                return Response::json([
                    'messege' => 'Something went wrong |try Again',
                    'errorCode' => '404'
                ], 404);
            }
        } else {
            return Response::json([
                'messege' => 'No Order found',
                'errorCode' => '404'
            ], 404);
        }
    }
    public function quote_status_update(Request $request)
    {
        $order_id = $request->order_id;
        $status = $request->status;
        $comments = $request->comments;
        if ($order_id != '') {
            $data = array(
                'status' => $status,
            );
            $res1 = CommonModel::updateData('quotation', 'id', $data, $order_id);
            if ($status != 1) {
                if ($comments != "") {
                    $data = DB::table('quote_comments')->insertGetId([
                        'customer_id' => $request->customer_id,
                        'order_id' => $order_id,
                        'comments' => $comments,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                }
            } else {
                $quote = Quotes::findorFail($order_id);
                $data = DB::table('order')->insertGetId([
                    'quote_id' => $order_id,
                    'est_id' => $quote->est_id,
                    'cust_id' => $quote->cust_id,
                    'supl_id' => $quote->supl_id,
                    'order_no' => $quote->order_no,
                    'order_date' => date('Y-m-d'),
                    'discount' => $quote->discount,
                    'discount_per' => $quote->discount_per,
                    'tax' => $quote->tax,
                    'tax_per' => $quote->tax_per,
                    'total' => $quote->total
                ]);
                @$contact = Contact::findorFail($quote->cust_id);
                
                $data = array('name'=>"OMS", 'body' => "A test mail");
                $to_name = $contact->name;
                $to_email = $contact->email;
                Mail::send('mail', $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                    ->subject('Quotation Update');
                    $message->from('tofiqkharadi@gmail.com','Quotation Update');
                    });
            }
            return response()->json(['status' => true, 'message' => "Status Update Successfully"]);
        } else {
            return response()->json(['status' => false, 'message' => "Order id Required"]);
        }
    }
    public function user_dashboard()
    {
        $data['company_count'] = DB::table('companies')->count();
        $data['contact_count'] = DB::table('contacts')->count();
        $data['order_count'] = DB::table('order')->count();
        $data['products_count'] = DB::table('products')->count();
        $data['companies'] = DB::table('companies')->orderBy('id', 'desc')->take(5)->get();
        return response()->json([
            'data' => $data,
            'message' => 'Dashboard get successfully',
        ], 200);
    }

}
