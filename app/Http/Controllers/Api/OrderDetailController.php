<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;
use Validator;
use App\Models\OrderDetail;

class OrderDetailController extends Controller
{
       /**
* Function to Api of  add.
*
* @return Response
*/
    public function insert_order_product(Request $request)
    { 
       $validatedData = Validator::make($request->all(),[
            'order_id' => 'required',
            'product_id'=> 'required',
            'total'=> 'required',
        ]);

        if ($validatedData->fails()) {   
        return Response::json([   
                'status' => false,
                'messege' => $validatedData->messages()->all(),
                'data' => []  
            ], 404);
        }       
        if ($validatedData)
        { 
            $data = OrderDetail::create($request->all());
            if (!$data) {
                return Response::json([
                    'status' => false,
                    'message' => 'Something went wrong |try Again',
                    'data' => []   
                ], 404);
            } else {   
                return Response::json([
                    'status' => true,
                    'message' => 'Product Added Successfully',
                    'data' => $data,
                ], 200);
            }
        }
    }
    public function show($id)
    {
        $data = OrderDetail::findorFail($id); //searches for the object in the database using its id and returns it.

        return response()->json([
            'data' => $data,
            'message' => 'Product get successfully',
        ], 200);
    }
    public function update(Request $request,$id)
    { 
        $validatedData = Validator::make($request->all(),[
            'order_id' => 'required',
            'product_id'=> 'required',
            'total'=> 'required',
        ]);
        if ($validatedData->fails()) { 
           return Response::json([   
                            'status' => false,
                            'messege' => $validatedData->messages()->all(),
                            'data' => []
                        ], 404);
        }
        if ($validatedData)
        { 
            $data = OrderDetail::find($id);               
            if (!$data->update($request->all())) {
                return Response::json([  
                        'status'  => false,
                        'messege' => 'Something went wrong |try Again',
                        'data' => [] ], 404);
            } else {   
                return Response::json([
                    'status'  => true,
                    'messege' => 'Product Updated Successfully',
                    'data' => $data
                ], 200);
             }
        }
    }
    public function order_product_list(Request $request,$id)
    {
        $data = OrderDetail::where('order_id',$id)->get();              
        if (count($data)> 0 ) {
            return Response::json([
                'status'=>true,
                'messege' => 'Products List found Successfully',
                'data' => $data,
            ], 200);
        } else {              
            return Response::json([    
                'status'=>false,               
                'messege' => 'No Products found',
                'errorCode' => '404' ], 404);
         }
    }
    public function destroy(Request $request,$id)
    {   
        if ($id >0) {           
           if(OrderDetail::find($id)->delete()){
            return Response::json([
                'Message' => '',
                'data' => 'Data Deleted Successfully',
            ], 200);
           } else {
                return Response::json([                   
                'messege' => 'Something went wrong |try Again',
                'errorCode' => '404' ], 404);
           }           
        } else {             
            return Response::json([                   
                'messege' => 'No Product found',
                'errorCode' => '404' ], 404);
         }
    }
}
