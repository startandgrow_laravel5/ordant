<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;
use Validator;
use App\Models\Contact;
use DB;
class ContactController extends Controller
{
    /**
     * Function to Api of spacopmlex add.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'name' => 'required',
            'company_id' => 'required',
            'email' => 'required|unique:contacts',
            'country' => 'required',
            'head_office_number' => 'required',
        ]);
        if ($validatedData->fails()) {
            return Response::json([
                'status' => false,
                'messege' => $validatedData->messages()->all(),
                'data' => []
            ], 404);
        }

        if ($validatedData) {
            $contact = Contact::create($request->all());
            if (!$contact) {
                return Response::json([
                    'status' => false,
                    'message' => 'Something went wrong |try Again',
                    'data' => []
                ], 404);
            } else {
                $data = DB::table('users')->insertGetId([
                    'fname' => $request->name,
                    'lname' => $request->name,
                    'username' => $request->name,
                    'email' => $request->email,
                    'password' => bcrypt('root'),
                    'contact_number' => $request->head_office_number,
                    'country_id' => $request->country,
                    'region_id' => 1,
                    'town_id' => 1,
                    'address_line_1' => "Lorem Ipsum",
                    'postcode' => 00000
                ]);

                return Response::json([
                    'status' => true,
                    'message' => 'Contact Added Successfully',
                    'data' => $contact,
                ], 200);
            }
        }
    }

    public function show($id)
    {
        $contact = Contact::findorFail($id); //searches for the object in the database using its id and returns it.

        return response()->json([
            'data' => $contact,
            'message' => 'Contact get successfully',
        ], 200);
    }
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make($request->all(), [
            'name' => 'required',
            'company_id' => 'required',
            'email' => 'required|unique:contacts',
            'country' => 'required',
            'head_office_number' => 'required',
        ]);

        if ($validatedData->fails()) {
            return Response::json([
                'status' => false,
                'messege' => $validatedData->messages()->all(),
                'data' => []
            ], 404);
        }

        if ($validatedData) {
            $contact = Contact::find($id);
            if (!$contact->update($request->all())) {
                return Response::json([
                    'status'  => false,
                    'messege' => 'Something went wrong |try Again',
                    'data' => []
                ], 404);
            } else {
                return Response::json([
                    'status'  => true,
                    'messege' => 'Contact Updated Successfully',
                    'data' => $contact
                ], 200);
            }
        }
    }
    public function index(Request $request)
    {
        $contact = Contact::orderBy('id', 'DESC')->paginate(15);
        if (count($contact) > 0) {
            return Response::json([
                'status' => true,
                'message' => 'Contact List Found Successfully',
                'data' => $contact,
            ], 200);
        } else {
            return Response::json([
                'status' => false,
                'messege' => 'No contacts found',
                'errorCode' => '404'
            ], 404);
        }
    }
    public function destroy(Request $request, $id)
    {
        if ($id > 0) {
            if (Contact::find($id)->delete()) {
                return Response::json([
                    'status' => true,
                    'Message' => 'Data Deleted Successfully'
                ], 200);
            } else {
                return Response::json([
                    'status' => false,
                    'messege' => 'Something went wrong |try Again'
                ], 404);
            }
        } else {
            return Response::json([
                'status' => false,
                'messege' => 'No Contacts found'
            ], 404);
        }
    }
}
