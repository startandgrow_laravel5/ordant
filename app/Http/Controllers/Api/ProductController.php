<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Models\Api\CommonModel;
use Carbon\Carbon;
use Validator;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Function to Api of  add.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'product_name' => 'required',
            'product_size' => 'required',
            'quantity' => 'required|numeric',
            'vendor_price' => 'required|numeric',
            'product_price' => 'required|numeric',
            'artwork_name' => 'required',
        ]);

        if ($validatedData->fails()) {
            return Response::json([
                'status' => false,
                'messege' => $validatedData->messages()->all(),
                'data' => []
            ], 404);
        }
        if ($validatedData) {
            $product = Product::create($request->all());
            if (!$product) {
                return Response::json([
                    'status' => false,
                    'message' => 'Something went wrong |try Again',
                    'data' => []
                ], 404);
            } else {
                return Response::json([
                    'status' => true,
                    'message' => 'Product Added Successfully',
                    'data' => $product,
                ], 200);
            }
        }
    }
    public function product_search(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'key' => 'required|alpha_dash|min:3',
        ]);
        if ($validatedData->fails()) {
            return Response::json([
                'status' => false,
                'messege' => $validatedData->messages()->all(),
                'data' => []
            ], 404);
        }
        if ($validatedData) {
            $product = CommonModel::getSearch($request->key)->all();
            if ($product != "") {
                $prod = [];
                for ($i = 0; $i < count($product); $i++) {
                    $d['id'] = @$product[$i]->id;
                    $d['product_name'] =   @$product[$i]->product_name;
                    $d['product_size'] =  @$product[$i]->product_size;
                    $d['quantity'] = @$product[$i]->quantity;
                    $d['product_option'] =  @$product[$i]->product_option;
                    $d['vendor_price'] =  @$product[$i]->vendor_price;
                    $d['product_price'] =  @$product[$i]->product_price;
                    $d['product_weight'] =  @$product[$i]->product_weight;
                    $d['product_days'] =  @$product[$i]->product_days;
                    $d['product_sku'] =  @$product[$i]->product_sku;
                    $d['artwork_name'] = @$product[$i]->artwork_name;
                    $d['product_image'] = @$product[$i]->product_image;
                    $d['status'] =  @$product[$i]->status;
                    $prod[] = $d;
                }
                $products = $prod;
            } else {
                $products = [];
            }
            $data['product'] = $products;
            return response()->json(['status' => true, 'message' => "Search Successfully", 'data' => $data]);
        }
    }
    public function show($id)
    {
        $product = Product::findorFail($id); //searches for the object in the database using its id and returns it.

        return response()->json([
            'data' => $product,
            'message' => 'Product get successfully',
        ], 200);
    }
    public function update(Request $request)
    {

        $input  = $request->all();
        $validatedData = Validator::make($request->all(), [
            'product_name' => 'required',
            'product_size' => 'required',
            'quantity' => 'required|numeric',
            'vendor_price' => 'required|numeric',
            'product_price' => 'required|numeric',
            //'artwork_name' => 'required',
            'status' => 'required',
        ]);
        if ($validatedData->fails()) {
            return Response::json([
                'status' => false,
                'messege' => $validatedData->messages()->all(),
                'data' => []
            ], 404);
        }
        if ($validatedData) {
            $product = Product::find($input['id']);


            $icon_file = '';
            if($request->hasFile('product_image')){

                $file   = $request->file('product_image');
                $ds     = public_path('uploads/product');
                if(!is_dir($ds)){
                    @mkdir($ds, 0777, true);
                }
                // Delete old file
                if(file_exists($ds.'/'.$product->product_image)){
                    @unlink($ds.'/'.$product->product_image);
                }
                $fileName   = date('Ymdhis').rand(1,999).'-product.'.$file->getClientOriginalExtension();
                $file->move($ds,$fileName);

                $input['product_image'] = 'uploads/product/'.$fileName;
            }else{
                unset($input['product_image']);
            }

            if (!$product->update($input)) {
                return Response::json([
                    'status'  => false,
                    'messege' => 'Something went wrong |try Again',
                    'data' => []
                ], 404);
            } else {
                return Response::json([
                    'status'  => true,
                    'messege' => 'Product Updated Successfully',
                    'data' => $product
                ], 200);
            }
        }
    }
    public function index(Request $request)
    {
        $contact = Product::orderBy('id', 'DESC')->paginate(15);
        if (count($contact) > 0) {
            return Response::json([
                'status' => true,
                'messege' => 'Products List found Successfully',
                'data' => $contact,
            ], 200);
        } else {
            return Response::json([
                'status' => false,
                'messege' => 'No Products found',
                'errorCode' => '404'
            ], 404);
        }
    }
    public function destroy(Request $request, $id)
    {
        if ($id > 0) {
            if (Product::find($id)->delete()) {
                return Response::json([
                    'Message' => '',
                    'data' => 'Data Deleted Successfully',
                ], 200);
            } else {
                return Response::json([
                    'messege' => 'Something went wrong |try Again',
                    'errorCode' => '404'
                ], 404);
            }
        } else {
            return Response::json([
                'messege' => 'No Product found',
                'errorCode' => '404'
            ], 404);
        }
    }
}
