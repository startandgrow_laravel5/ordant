<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Quotes;
use PDF;
use App\Exports\QuoteExport;
use Maatwebsite\Excel\Facades\Excel;


class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('quote.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('quote.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function quotePDF()
    {
        $data = Quotes::select('quotation')
        ->leftJoin('companies', 'companies.id', '=', 'quotation.supl_id')
        ->leftJoin('contacts', 'contacts.id', '=', 'quotation.cust_id')
        ->leftJoin('estimates', 'estimates.id', '=', 'quotation.est_id')
        ->select('quotation.*', 'companies.name as supplier_name', 'companies.id as company_id', 'estimates.project_name', 'contacts.name as customer_name')
        ->orderBy('quotation.id', 'DESC')
        ->get();
        view()->share('quote', $data);
        $pdf = PDF::loadView('pdf.quote', $data);
        return $pdf->download('pdf_file.pdf');
    }
    public function quoteCSV()
    {
        return Excel::download(new QuoteExport, date('Y-m-d H:i:s').'quote.csv');
    }
}