<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Api\CommonModel;

use Illuminate\Http\Request;
use PDF;
use App\Exports\CompanyExport;
use Maatwebsite\Excel\Facades\Excel;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data= Company::all();
        return view('company.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.add');
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = $id;
        if ($id != "") {
            $data = CommonModel::getDataById('companies', 'id', $id)->first();
            if ($data != "") {
                $company = $data;
            } else {
                $company = [];
            }
            
        return view('company.edit',compact('company'));
    }
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
    public function companyCSV()
    {
        return Excel::download(new CompanyExport, date('Y-m-d H:i:s').'company.csv');
    }
    public function companyPDF()
    {
        $data = Company::all();
        view()->share('company', $data);
        $pdf = PDF::loadView('pdf.company', $data);
        return $pdf->download('company.pdf');
    }
}
