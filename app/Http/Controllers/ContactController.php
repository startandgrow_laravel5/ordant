<?php

namespace App\Http\Controllers;
use App\Models\Contact;
use Illuminate\Http\Request;
use PDF;
use App\Exports\ContactExport;
use Maatwebsite\Excel\Facades\Excel;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = $id;
        if ($id != "") {
            $data = Contact::findorFail($id);
            if ($data != "") {
                $company = $data;
            } else {
                $company = [];
            }
            
        return view('contact.edit',compact('company'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  
    public function contactCSV()
    {
        return Excel::download(new ContactExport, date('Y-m-d H:i:s').'contact.csv');
    }
    public function contactPDF()
    {
        $data = Contact::select('contacts')
        ->leftJoin('companies', 'companies.id', '=', 'contacts.company_id')
        ->select('contacts.*', 'companies.name as company_name')
        ->orderBy('contacts.id', 'DESC')
        ->get();
        view()->share('contact', $data);
        $pdf = PDF::loadView('pdf.contact', $data);
        return $pdf->download('pdf_file.pdf');
    }
}
