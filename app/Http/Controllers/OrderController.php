<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use PDF;
use App\Exports\OrderExport;
use Maatwebsite\Excel\Facades\Excel;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function orderPDF()
    {
        $data = Orders::select('order')
        ->leftJoin('companies', 'companies.id', '=', 'order.supl_id')
        ->leftJoin('contacts', 'contacts.id', '=', 'order.cust_id')
        ->leftJoin('estimates', 'estimates.id', '=', 'order.est_id')
        ->select('order.*', 'companies.name as supplier_name', 'companies.id', 'estimates.project_name', 'contacts.name as customer_name')
        ->orderBy('order.id', 'DESC')
        ->get();
        view()->share('order', $data);
        $pdf = PDF::loadView('pdf.order', $data);
        return $pdf->download('pdf_file.pdf');
    }
    public function orderCSV()
    {
        return Excel::download(new OrderExport, date('Y-m-d H:i:s').'order.csv');
    }
}
