<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\QuoteController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');


//Site Setting
Route::resource('/site-setting', 'App\Http\Controllers\SiteSettingController', [
    'names' => ['index' => 'site_setting', 'store' => 'site_setting.store', 'edit' => 'site_setting.edit', 'update' => 'site_setting.update', 'destroy' => 'site_setting.destroy']
]);


Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home');
    Route::resource('contact', ContactController::class);
    Route::resource('companies', CompanyController::class);
    Route::resource('orders', OrderController::class);
    Route::resource('product', ProductController::class);
    Route::resource('quotes', QuoteController::class);
    Route::resource('email-template', \App\Http\Controllers\EmailTemplateController::class);

//Export PDF
Route::get('order/order_pdf', [OrderController::class, 'orderPDF'])->name('order_pdf');
Route::get('quote/quote_pdf', [QuoteController::class, 'quotePDF'])->name('quote_pdf');
Route::get('company/company_pdf', [CompanyController::class, 'companyPDF'])->name('company_pdf');
Route::get('contacts/contact_pdf', [ContactController::class, 'contactPDF'])->name('contact_pdf');
//Export CSV
Route::get('order/order_csv', [OrderController::class, 'orderCSV'])->name('order_csv');
Route::get('quote/quote_csv', [QuoteController::class, 'quoteCSV'])->name('quote_csv');
Route::get('company/company_csv', [CompanyController::class, 'companyCSV'])->name('company_csv');
Route::get('contacts/contact_csv', [ContactController::class, 'contactCSV'])->name('contact_csv');
//Mail Send
Route::get('sendbasicemail', [MailController::class, 'basic_email'])->name('sendbasicemail');
Route::get('sendhtmlemail', [MailController::class, 'html_email'])->name('sendhtmlemail');
Route::get('sendattachmentemail', [MailController::class, 'attachment_email'])->name('sendattachmentemail');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
