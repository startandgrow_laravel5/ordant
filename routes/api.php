<?php

  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Route;
  use App\Http\Controllers\api\UserController;
  use App\Http\Controllers\api\CompanyController;
  use App\Http\Controllers\api\ContactController;
  use App\Http\Controllers\api\ProductController;



  /*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login-user', [UserController::class, 'loginUser']);
Route::post('/register-user', [UserController::class, 'registerUser']);


//Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::post('/user', [UserController::class, 'userdetails']);
        Route::post('/log-out', [UserController::class, 'logout']);
        Route::post('/verify-Otp', [UserController::class, 'verifyOtp']);
       
//});

 

  //---login---
  Route::post('/register', 'Api\AuthController@register');
  Route::post('/login', 'App\Http\Controllers\Api\AuthController@login');
Route::post('/logout', 'Api\AuthController@logout');

  Route::group(['prefix' => '/', 'namespace' => 'App\Http\Controllers\Api'], function () {

    //Route::group(['middleware' => ['auth:sanctum']], function () {

      //---Dashboard---
      Route::get('user_dashboard', 'OrderController@user_dashboard');
      //---Company---
      Route::get('company_list', 'CompanyController@company_list');
      Route::get('company_detail/{id}', 'CompanyController@company_detail');
      Route::put('company_update', 'CompanyController@company_update');
      Route::post('insert_company', 'CompanyController@insert_company');
      //---Quotes---
      Route::get('quote_list/{id}', 'OrderController@quote_list');
      Route::get('order_list/{id}', 'OrderController@order_list');
      Route::get('order_detail/{id}', 'OrderController@order_detail');
      Route::put('order_update', 'OrderController@order_update');
      Route::post('order_insert', 'OrderController@order_insert');
      Route::get('order_product_list/{id}', 'OrderDetailController@order_product_list');
      Route::post('insert_order_product', 'OrderDetailController@insert_order_product');
      //---Product Search---
      Route::post('product_search', 'ProductController@product_search');
      //---Order Comment List---
      Route::get('comment_list/{id}', 'OrderController@comment_list');
      //---Status Update---
      Route::post('quote_status_update', 'OrderController@quote_status_update');
      //---Country List---
      Route::get('country_list', 'CompanyController@country_list');
   // });

      Route::post('product-update', 'ProductController@update');


      Route::get('template-list', 'EmailTemplateController@index');
      Route::post('add-emailtemplate', 'EmailTemplateController@store');
      Route::post('edit-emailtemplate', 'EmailTemplateController@update');

      


  //  });
  });


  //Route::group(['middleware' => ['auth:sanctum']], function () {

  Route::apiResource('contacts', ContactController::class);
  Route::apiResource('products', ProductController::class);

  Route::get('/companies', [CompanyController::class, 'index']);
  Route::post('/companies', [CompanyController::class, 'store']);
  Route::post('/companies/{id}/edit', [CompanyController::class, 'update']);
  Route::delete('/companies/destroy/{id}', [CompanyController::class, 'destroy']);
//});
